<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans&display=swap" rel="stylesheet">
</head>
<body>
	<div style="background:#f5f5f5;max-width: 660px; margin: 0px auto; padding: 50px;padding-top: 27px;border-bottom: 1px solid #ebebeb;font-family: 'Noto Sans', sans-serif;">
		<div style="float: left;width: 100%;padding-bottom: 20px;">
			<div style="float: left;width: 50%; ">
				<div style="padding-left: 50px;padding-top: 17px;">
					<a href="https://fime.vn/" target="_blank"><img src="{{asset('images/Logo.png')}}"></a>
				</div>
				
			</div>
			<div style="float: right;width: 50%;text-align: right;">
				<img src="{{asset('images/mail.png')}}">
			</div>
		</div>
		<div style="background: #fff;padding: 50px 57px; text-align: center;clear: both;border-bottom: 1px solid #ebebeb;">
			<h2 style="font-size: 24px;font-weight: bold;color: #000000">Cảm ơn bạn đã đăng ký tài khoản tại <a style="font-size: 24px;font-weight: bold;color: #000000;text-decoration: none;" href="https://fime.vn/">fime.vn!</a></h2>
			<h2 style="font-size: 24px;font-weight: bold;color: #000000;">Xin vui lòng bấm vào <a href="{{$url}}" style="color:#f14c7f;text-decoration: underline;">liên kết này</a> để trở thành một fi :mer chính hiệu</h2>
			<p style="font-size: 20px;color: #010101;margin: 0px;line-height: 24px;">Thank you for registering!</p>
			<p style="font-size: 20px;color: #010101;margin: 0px;line-height: 24px;">Please check <a href="{{$url}}" style="color:#f14c7f;text-decoration: underline;">this link</a> to become one of unique members.</p>
		</div>
		<div style="background: #fff;padding: 50px;">
			<ul style="margin: 0px;padding-left: 7px;">
				<li>
					<p style="font-size: 16px;color: #151515;margin: 0px;line-height: 24px;">Bạn có thể sẽ không sử dụng được một số tính năng nếu không thực hiện việc xác thực e-mail này.</p>
					<p style="font-size: 16px;color: #666666;font-style: italic;margin: 0px;line-height: 24px;">You may not use some functions if you do not complete e-mail verification.</p>
				</li>
				<li>
					<p style="font-size: 16px;color: #151515;margin: 0px;line-height: 24px;">E-mail này chỉ khả dụng trong vòng 24 giờ.</p>
					<p style="font-size: 16px;color: #666666;font-style: italic;margin: 0px;line-height: 24px;">This e-mail is only available for 24 hours.</p>
				</li>
			</ul>
		</div>
		<div style="padding-left: 20px; max-width: 560px;    padding-top: 20px;">
			<p style="font-size: 14px;color: #8a8a8a;margin: 0px;line-height: 24px;">Trường hợp bạn nhận được e-mail này nhưng không thực hiện việc đăng ký tại <a href="" style="font-size: 14px;color: #8a8a8a;margin: 0px;line-height: 24px;text-decoration: none;"> fime.vn,</a> <a href="mailto:info.fime@dmnc.vn" style="color: #0018ff;text-decoration: underline;">vui lòng thông báo</a> với chúng tôi.</p>
			<p style="font-size: 14px;color: #adadad;line-height: 24px;margin: 0px;font-style: italic;">If you receive this e-mail but did not register your account at <a href="" style="font-size: 14px;color: #adadad;margin: 0px;line-height: 24px;text-decoration: none;"> fime.vn,</a> please <a href="mailto:info.fime@dmnc.vn" style="color: #0018ff;text-decoration: underline;">click here</a>.</p>
		</div>
		<div style="padding-left: 20px;">
			<p style="font-size: 14px;color: #8a8a8a;margin: 0px;line-height: 24px;">Đây là e-mail trả lời tự động. Nếu cần trao đổi trực tiếp, vui lòng liên hệ: <a href="mailto:info.fime@dmnc.vn" style="color: #0018ff;text-decoration: underline;margin: 0px;">info.fime@dmnc.vn</a>.</p>
			<p style="font-size: 14px;color: #adadad;margin: 0px;line-height: 24px;font-style: italic;">This is no reply e-mail. To make an inquiry, please contact: <a href="mailto:info.fime@dmnc.vn" style="color: #0018ff;text-decoration: underline;">info.fime@dmnc.vn</a>.</p>
		</div>
	</div>
	<div style="background:#f5f5f5;max-width: 660px; margin: 0px auto; padding: 20px 50px;font-family: 'Noto Sans', sans-serif;">

		<div style="padding-left: 20px;">
			<p style="font-size: 12px;margin: 0px;line-height: 24px;color: #999999">Copyright © DM&C. All rights reserved.</p>
			<p style="font-size: 12px;margin: 0px;line-height: 24px;color: #999999"><strong>Công Ty</strong> TNHH DM&C</p>
			<p style="font-size: 12px;margin: 0px;line-height: 24px;color: #999999">Tầng 10, 194 Golden Building, 473 Điện Biên Phủ, phường 25, quận Bình Thạnh, thành phố Hồ Chí Minh.</p>
		</div>
	</div>
</body>
</html>