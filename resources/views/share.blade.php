<html lang="ko">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="True">
    <title>fime</title>
    <meta property="og:url" content="https://api.fime.vn/">
	<meta property="og:type" content="article">
	<meta property="og:title" content="fi:me">
	<meta property="og:title" content="fi:me">
	<meta property="og:description" content="Fime: Shine Your Style">
	<meta property="og:image" content="http://api.fime.vn/images/appstore.png">
	<meta property="fb:app_id" content="197684317620676">
    <link rel="stylesheet" href="http://api.fime.vn/css/normalize.css">
    <link rel="stylesheet" href="http://api.fime.vn/css/style.css">
	<link rel="stylesheet" href="http://api.fime.vn/css/reset.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.2.3/jquery.min.js"></script>

    <script type="text/javascript">
		$(document).ready(function(){     
			var navCase = navigator.userAgent.toLocaleLowerCase(); //접속기기정보가져오기

			var nowAddress = unescape(location.href);
			var parameters = nowAddress.substring(nowAddress.indexOf('?')+1,nowAddress.length);      	
			var appSchem = 'fime://'+parameters;     		

			if(navCase.search("android") > -1){  //안드로이드일경우			
				$(".googleplay").show();
				location.href = "Intent://" + parameters + "#Intent;scheme=fime;package=vn.fime.dmnc.method;end";
			}else{
				$(".appstore").show();
				location.href = "fime://" + parameters;
			}
		});		
    </script>
</head>
<body>
    <div class="wrapper">
        <div class="container">
		<form id="frm" name="frm" method="get" action="">
            <div class="header">               
                <h1><a href="#"><img src="http://api.fime.vn/images/logo.png" alt=""></a></h1>
                <p>Cài đặt "fi: me Ứng dụng di động".</p>
            </div>           
            <div class="btn">
                <a href="#" style="display:none;" class="googleplay"></a>
                <a href="#" style="display:none;" class="appstore"></a>				
            </div>
		</form>
        </div>        
	</div>		

	<!-- <form id="frm2" name="frm2" action="/api/goods/reviewWrite" enctype="multipart/form-data" method="post">
		<input type="text" name="UserNo" value="U20190513115310"/>
		<input type="text" name="title" value="test"/>
		<input type="text" name="contents" value="test"/>
		<input type="text" name="goods_cl_code" value="407001"/>
		<input type="text" name="goods_nm" value="test"/>
		<input type="file" name="add_files1"/>
		<input type="button" value="전송" onclick="document.frm2.submit()">
	</form> -->
</body>
</html>