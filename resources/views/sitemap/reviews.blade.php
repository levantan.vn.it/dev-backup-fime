<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($reviews as $review)
    <url>
        <loc>http://fime.vn/reviews/detail/{{$review->slug}}</loc>
        <lastmod>{{$review->writng_dt}}</lastmod>
        <changefreq>always</changefreq>
        <priority>1</priority>
    </url>
    @endforeach
</urlset>