<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	dd("welcome");
    return view('welcome');
});

Route::get('/share', function () {
    return view('share');
});

Route::get('/sitemap.xml', 'SitemapController@index');
Route::get('/sitemap.xml/tries', 'SitemapController@tries');
Route::get('/sitemap.xml/reviews', 'SitemapController@reviews');
Route::get('/sitemap.xml/tips', 'SitemapController@tips');