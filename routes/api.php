<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use App\Models\Review;
use App\Models\TryFree;
use App\Models\Hashtag;
use App\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
Route::get('check_try', function(){
    echo phpinfo();
});
// Verify User
Route::get('check_update/{email}', function($email){
    $user = User::where('email',$email)->first();
    // $user->delete();
    dd($user);
    // $user->access_token = "kXl6Pt9UIk";
    $user->verification = 0;
    $user->save();
    dd($user);
});

Route::get('create_user', function(){
    $newUserIdentity = new User();
    $newUserIdentity->user_no = "U20200120120207";
    $newUserIdentity->sns_id = "113610206799395";
    $newUserIdentity->reg_name = "Blueming Poem";
    $newUserIdentity->home_zip = "NP000";
    $newUserIdentity->verification = 0;
    $newUserIdentity->save();
    dd($newUserIdentity);
});
Route::get('update_review_user', function(){
    $user = User::where('update_review','<>',1)->limit(1000)->get();
    if(!empty($user) && count($user)){
        foreach ($user as $key) {
            $total_review = Review::select(\DB::raw('count(*) as number_of_reviews'), 'user_no as user_id')
                    ->where('TCT_REVIEW.delete_at', 'N')
                    ->where('TCT_REVIEW.expsr_at', 'Y')
                    ->where('user_no', $key->user_no)
                    ->groupBy('user_no')->first();
            if(!empty($total_review->number_of_reviews) && $total_review->number_of_reviews > 0)
                $key->reviews = $total_review->number_of_reviews;
            else
                $key->reviews = 0;
            $key->update_review = 1;
            $key->save();
            
        }
        dd('OK');
    }
    dd('Done');
    
});
Route::get('add_brand_fl', function(){
    Schema::create('TCT_BRFL', function($table){
        $table->string('user_no');
        $table->string('fllwr_br');
        $table->dateTime('regist_dt');
    });
    dd('OK');
});
Route::get('add_image', function(){
    Schema::table('TCT_GOODS_ANWR', function (Blueprint $table) {
        $table->string('resource_type')->nullable();
    });
    dd('OK');
});

Route::get('add_brand_tip', function(){
    Schema::table('TCT_TIPS', function (Blueprint $table) {
        $table->string('code_brand')->nullable();
    });
    dd('OK');
});

Route::get('add_banner_view', function(){
    Schema::table('banners', function (Blueprint $table) {
        $table->integer('view_cnt')->default(0);
    });
    dd('OK');
});

Route::get('add_ads_view', function(){
    Schema::table('ads', function (Blueprint $table) {
        // $table->dropColumn('view_cnt');
        $table->string('view_cnt')->nullable()->default(0);
    });
    dd('OK');
});

// TCT_GOODS
Route::get('add_review_view', function(){
    Schema::table('TCT_REVIEW', function (Blueprint $table) {
        // $table->dropColumn('view_cnt');
        $table->string('view_cnt')->nullable()->default(0);
    });
    dd('OK');
});

Route::get('add_image_brand', function(){
    Schema::table('TSM_CODE', function (Blueprint $table) {
        $table->string('file')->nullable();
    });
    dd('OK');
});

Route::get('add_user', function(){
    Schema::table('TDM_USER', function (Blueprint $table) {
        $table->integer('update_review')->default(0);
        // $table->integer('verification')->default(1);
        // $table->dateTime('verification_max')->nullable();
        // $table->dateTime('verification_at')->nullable();
    });
    $user = User::first();
    dd($user);
    dd('OK');
});
Route::get('check-hag', function(){
    // Schema::table('TCT_POPHASH', function (Blueprint $table) {
    //     $table->integer('status')->default(0);
    // });
    $hag = Hashtag::where('status',0)->limit(40)->orderBy('hash_cnt', 'desc')->update(['status'=>1]);
    dd($hag);
});
Route::get('update-review', function(){
    $reviews = Review::whereNull('slug')->orWhere("slug","")->get();
    // dd($reviews);
    if(!empty($reviews)){
        foreach ($reviews as $key) {
           $slug = str_slug($key->goods_nm,'-');
           if(empty($slug)){
                $slug = str_random(10);
                $check_slug = Review::where('slug',$slug)->first();
                if(!empty($check_slug)){
                    while (true) {
                        $slug = str_random(10);
                        $check_slug = Review::where('slug',$slug)->first();
                        if(empty($check_slug)){
                            break;
                        }
                    }
                }
                   
           }
           $key->slug = $slug;
           $a = $key->save();
        }
    }
    dd("OK");
});

Route::get('get-review', function(){
    $reviews = Review::select('*', DB::raw('count(slug) as total'))->groupBy('slug')->havingRaw("COUNT(slug) > 1")->get();
    dd($reviews);
    if(!empty($reviews)){
        foreach ($reviews as $value) {
            $slug = str_slug($value->goods_nm,'-');
            if(empty($slug)){
                $slug = str_random(10);
            }
            $check_slug = Review::where('slug',$slug)->first();
            if(!empty($check_slug)){
                while (true) {
                    $slug = str_random(10);
                    $check_slug = Review::where('slug',$slug)->first();
                    if(empty($check_slug)){
                        break;
                    }
                }
                
            }
            $value->slug = $slug;
            $value->save();
        }
    }
    dd("OK");
    
});

Route::get('find-review/slug/{slug}', function($slug){
    $reviews = Review::Where("slug",$slug)->first();
    dd($reviews);
    
    dd("OK");
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('updatePic', 'TryController@updatePic');
Route::prefix('auth')->group(function () {
    Route::post('login', 'Auth\AuthController@login');
    Route::post('loginByFacebook', 'Auth\AuthController@loginByFacebook');
    Route::post('logout', 'Auth\AuthController@logout');
    Route::post('register', 'Auth\AuthController@register');
    Route::post('resetPassword', 'Auth\AuthController@resetPassword');
    Route::post('forgotPassword', 'Auth\AuthController@forgotPassword');
    Route::post('getUserProfile', function () {
        return auth()->user();
    });
    Route::post('changePassword', 'Auth\AuthController@changePassword');
    
});
Route::get('verifyEmail', 'Auth\AuthController@verifyEmail');
Route::post('resendVerifyEmail', 'Auth\AuthController@resendVerifyEmail');

/*BANNER API*/
Route::post('uploads', 'UploadController@store');

/*End BANNER API*/

/*TRY API*/
Route::get('tries/getAvailableTries', 'TryController@getAvailableTries');
Route::get('tries/slug', 'TryController@getBySlug');
Route::get('tries/slug-ssr', 'TryController@getBySlugSSR');
Route::get('tries/all', 'TryController@getAllTries');
Route::get('tries/write-review/{id}', 'TryController@getBy');
Route::get('getTipByTryId/{id}', 'TryController@getTipByTryId');
Route::get('getReviewsByTry/{slug}', 'TryController@getReviewsByTry');
Route::get('viewBuyCount/{id}', 'TryController@viewBuyCount');

Route::get('user-tries/getShippings', 'UserTryController@getShippings');
Route::get('user-tries/getShippingStatuses', 'UserTryController@getShippingStatuses');
Route::post('user-tries', 'UserTryController@apply');
Route::post('user-tries/updateDeliveryInfo', 'UserTryController@updateDeliveryInfo');
/*End TRY API*/

/*BANNER API*/
Route::get('banners', 'BannerController@index');
Route::get('banners/get', 'BannerController@get');
Route::get('banners/all', 'BannerController@getAll');
Route::post('bannerView/{id}', 'BannerController@bannerView');

/*End BANNER API*/


/*BLOG API*/
Route::get('blogs', 'BlogController@index');
Route::get('blogs/getByPagination', 'BlogController@getByPagination');
Route::get('blogs/available', 'BlogController@available');
Route::get('blogs/latest', 'BlogController@latest');
Route::get('blogs/top-view', 'BlogController@getTopView');
Route::get('blogs/getBySlug', 'BlogController@getBySlug');
Route::get('blogs/{id}', 'BlogController@getBy');
Route::post('blogs', 'BlogController@store');
Route::post('blogs/deleteMulti', 'BlogController@deleteMulti');
Route::put('blogs/toggle', 'BlogController@toggle');
Route::put('blogs/{id}', 'BlogController@update');

/*End BLOG API*/

/*End Review Category API*/


/*Try Catalog API*/

Route::get('categories', 'CategoryController@index');
Route::get('getFashion', 'CategoryController@getFashion');
Route::get('getBeauty', 'CategoryController@getBeauty');
Route::put('categories/toggle', 'CategoryController@toggle');
Route::get('categories/{id}', 'CategoryController@getBy');
Route::post('categories', 'CategoryController@store');
Route::put('categories/{id}', 'CategoryController@update');
Route::delete('categories/{id}', 'CategoryController@delete');
Route::delete('del-categories/{id}', 'CategoryController@delete');

/*End Try Catalog API*/

/*Try Category API*/

Route::get('getAllCategories', 'CategoriesController@index');
Route::delete('delete-categories/{id}', 'CategoriesController@delete');
Route::put('update-categories/{id}', 'CategoriesController@update');
Route::post('add-categories', 'CategoriesController@store');

/*End Try Category API*/
Route::prefix('admin')->group(function () {
    Route::get('users', 'UsersController@getUsers');
    Route::post('user/change-password', 'UserController@changePassword');
});

/* SOCIAL USER API */
Route::post('social-user/update', 'SocialUserController@updateUserID');

/* End SOCIAL USER API */

/*ADS API*/
Route::get('ads', 'AdsController@index');
Route::get('ads/available', 'AdsController@available');
Route::get('ads/{id}', 'AdsController@getBy');
Route::post('ads', 'AdsController@store');
Route::put('ads/{id}/toggle', 'AdsController@toggle');
Route::put('ads/{id}', 'AdsController@update');
Route::post('AdsViews/{id}', 'AdsController@adsViews');
Route::delete('ads/{id}', 'AdsController@delete');

/*End ADS API*/

/* Review API*/
Route::get('reviews', 'ReviewController@index');
Route::post('reviews', 'ReviewController@store');
Route::get('reviews/getNew', 'ReviewController@getNew');
Route::get('reviews/getNewAfter', 'ReviewController@getNewAfter');
Route::get('reviews/getByType/{type}', 'ReviewController@getReviewByType');
Route::get('reviews/{id}', 'ReviewController@get');
Route::get('reviews/detail/{slug}', 'ReviewController@getDetail');
Route::get('reviews/details/{slug}', 'ReviewController@getDetails');
Route::get('getHashtagByReview/{id}', 'ReviewController@getHashtagByReview');
Route::post('reviews/{id}', 'ReviewController@delete');
Route::put('reviews/toggle', 'ReviewController@toggle');
Route::put('reviews/togglePopular', 'ReviewController@togglePopular');
Route::put('reviews/edit', 'ReviewController@update');
Route::post('reviews/delete', 'ReviewController@delete');
Route::post('reviews-restore', 'ReviewController@restore');
Route::post('reviews/status/{id}', 'ReviewController@status');

/*End Review API*/


/* FAQ API*/
Route::get('faqs', 'FAQController@index');
Route::get('faqs/{code}', 'FAQController@get');
Route::get('faqs/getByCategory/{code}', 'FAQController@available');
Route::post('faqs/delete', 'FAQController@delete');
Route::put('faqs/toggle', 'FAQController@toggle');
Route::put('faqs/{id}', 'FAQController@update');
Route::post('faqs', 'FAQController@store');

/*End FAQ API*/

/* FAQ Category API*/
Route::get('faq-categories', 'FAQCategoryController@index');
Route::get('faq-categories/{id}', 'FAQCategoryController@getBy');
Route::post('faq-categories', 'FAQCategoryController@store');
Route::put('faq-categories/{id}', 'FAQCategoryController@update');
Route::delete('faq-categories/{id}', 'FAQCategoryController@delete');

/* END FAQ Category API*/

/* Comment API*/
Route::get('comments/all/{id}', 'CommentController@getCommentsOfReview');
Route::get('comments/list', 'CommentController@getListComments');
Route::post('comments/delete', 'CommentController@delete');
Route::post('comments/create', 'CommentController@store');
//Route::get('comments/{id}', 'CommentController@getById');
Route::put('comments/{id}/toggle', 'CommentController@toggle');
/* End Comment API*/


/* Setting */
Route::get('settings', 'SettingController@index');
Route::get('settings/getByGroup', 'SettingController@getByGroup');
/* End Setting */

/* Follow user */
Route::post('user-follows', 'UserFollowController@toggle');
Route::get('user-follows/topInteractive', 'UserFollowController@getTopInteractive');
/* End follow user */

/* Fimers */
Route::get('fimers', 'UsersController@GetAllHotFimers');
Route::get('fimers/profile', 'UsersController@getUserProfile');
Route::get('fimers/get/{filterType}', 'UsersController@getFimers');
Route::get('fimers/getHot/{type}', 'UsersController@getHotFimers');
Route::get('fimers/getHotFimer', 'UsersController@getListHotFimers');
Route::put('fimers/toggle', 'UsersController@toggle');
/* End Fimers */

/* Like action */
Route::post('user-likes', 'UserLikeController@toggle');
Route::get('user-likes/get-list', 'UserLikeController@getList');
/* End like action */

/* Like action */
Route::get('hashtags', 'HashtagController@getTopHashtags');
Route::get('hashtag', 'HashtagController@index');
Route::post('hashtag', 'HashtagController@store');
Route::put('hashtag/toggle', 'HashtagController@toggle');
Route::put('hashtag/{id}', 'HashtagController@update');
Route::delete('hashtag/{id}', 'HashtagController@delete');


/* End like action */

/* My page */
Route::get('usr/{slug}', 'MyPageController@get');
Route::get('usr/get/{user_no}', 'UserController@get');
Route::get('usr-info/reviews', 'MyPageController@getReviews');
Route::get('usr-info/likes', 'MyPageController@getLikeReviews');
Route::get('usr-info/followers', 'MyPageController@getFollowers');
Route::get('usr-info/followings', 'MyPageController@getFollowings');
Route::get('usr-info/tries', 'MyPageController@getTries');
Route::post('usr-info/follow', 'MyPageController@follow');
Route::post('my-page/edit', 'MyPageController@edit');
Route::post('my-page/linkFacebook', 'MyPageController@linkFacebook');
/* End my page */

/* Search page */
Route::get('search/tries', 'SearchPageController@getTries');
Route::get('search/fimers', 'SearchPageController@getFimers');
Route::get('search/tips', 'SearchPageController@getTips');
/* End search page */

/* System notification API*/
Route::post('system-notification', 'SystemNotificationController@store');
Route::get('system-notification', 'SystemNotificationController@index');
Route::put('system-notification/toggle', 'SystemNotificationController@toggle');
Route::put('system-notification/update', 'SystemNotificationController@update');
Route::post('system-notification/delete', 'SystemNotificationController@delete');
Route::get('system-notification/{id}', 'SystemNotificationController@getById');
/* End System notification API*/

/* System notification API*/
Route::get('notifications', 'NotificationController@getByUserId');
Route::get('notifications/number', 'NotificationController@getNotificationNumber');
Route::post('notifications/mark-as-seen', 'NotificationController@markNotificationAsSeen');
Route::put('notifications/mark-all-as-seen', 'NotificationController@markAllAsSeen');
/* End System notification API*/


/* Tips API */
Route::get('tips', 'TipController@getAll');
Route::get('tip/{id}', 'TipController@getByID');
Route::get('tips/top-view', 'TipController@getTopView');
Route::get('text-colors', 'TextColorController@index');
Route::get('tips-category', 'TipController@getCategory');
Route::get('tips-catalog', 'TipController@getTipCategory');
Route::get('tip-list', 'TipController@index');
Route::get('tips/{id}', 'TipController@getTipById');
Route::post('add-tips', 'TipController@store');
Route::put('tips/toggle', 'TipController@toggle');
Route::post('tips/deleteMulti', 'TipController@deleteMulti');
Route::post('uploads-tips', 'TipController@uploadTips');
Route::put('tips-edit/{id}', 'TipController@update');
Route::get('try-list', 'TipController@getListTries');
Route::get('GetTryById/{id}', 'TipController@GetTryById');
Route::get('GetListReview', 'TipController@GetListReview');
Route::get('GetReviewsTipMapping/{id}', 'TipController@GetReviewsTipMapping');
Route::get('GetTryTipMapping/{id}', 'TipController@GetTryTipMapping');
/* End Tips API */

/* Brands API */
Route::get('brands/getAll', 'BrandController@index');
Route::get('brand/{id}', 'BrandController@getById');
Route::get('brand/tries/{id}', 'TryController@GetTriesByBrandId');
Route::get('brand/products/{id}', 'TryController@getAllProducts');
Route::get('brand/reviews/{id}', 'BrandController@getReviewsByBrand');
Route::get('brand/tips/{id}', 'BrandController@getTipsByBrand');
/* End Brands API */

Route::post('brand-follows', 'BrandFollowController@toggle');
Route::get('brand-follows/count/{id}', 'BrandFollowController@getBrandFollow');
Route::get('brand-follows/check/{id}', 'BrandFollowController@checkBrandFollow');

Route::get('/sitemap.xml', 'SitemapController@index');
Route::get('/sitemap.xml/tries', 'SitemapController@tries');
Route::get('/sitemap.xml/reviews', 'SitemapController@reviews');
Route::get('/sitemap.xml/reviews1', 'SitemapController@reviews1');
Route::get('/sitemap.xml/tips', 'SitemapController@tips');
Route::get('/sitemap.xml/fimers', 'SitemapController@fimers');
Route::get('/sitemap.xml/fimers1', 'SitemapController@fimers1');
Route::get('/sitemap.xml/fimers2', 'SitemapController@fimers2');
Route::get('/sitemap.xml/fimers3', 'SitemapController@fimers3');
Route::get('/sitemap.xml/fimers4', 'SitemapController@fimers4');
Route::get('/sitemap.xml/categories', 'SitemapController@categories');
Route::post('sitemap.xml/create', 'SitemapController@sitemap');

Route::group(['middleware' => ['role:admin']], function () {
    Route::get('banners/{id}', 'BannerController@getBy');
    Route::post('banners', 'BannerController@store');
    Route::post('banners/deleteMulti', 'BannerController@deleteMulti');
    Route::put('banners/toggle', 'BannerController@toggle');
    Route::put('banners/{id}', 'BannerController@update');


    Route::get('tries', 'TryController@index');
    Route::get('tries/getByPagination', 'TryController@getByPagination');
    Route::get('tries/getTriesEnd', 'TryController@getTriesEnd');
    Route::get('tries/getTriesDeliveryAction', 'TryController@GetTriesDeliveryAction');
    Route::post('tries', 'TryController@store');
    Route::post('tries/deleteMulti', 'TryController@deleteMulti');
    Route::put('tries/toggle', 'TryController@toggle');
    Route::put('tries/{id}', 'TryController@update');
    Route::get('tries/{id}', 'TryController@getBy');
    

    Route::get('user-tries', 'UserTryController@getAll');
    Route::post('user-tries/toggle', 'UserTryController@toggle');


    Route::prefix('admin')->group(function () {
        Route::prefix('users')->group(function () {
            Route::post('get-list', 'UsersController@getList');
            Route::get('get-list', 'UsersController@getList');
        });

        Route::prefix('user')->group(function () {
            Route::get('searchByName', 'UserController@searchByName');
            Route::get('get/{user_no}', 'UserController@get');
            Route::delete('{user_no}', 'UserController@delete');
            Route::post('getByIds', 'UserController@getByIds');
            Route::post('{user_no}/updateStatus', 'UserController@updateStatus');
            Route::post('{user_no}/update', 'UserController@update');
            Route::post('/add', 'UserController@store');
        });

        Route::prefix('roles')->group(function () {
            Route::get('get-list', 'RoleController@getList');
        });

        Route::prefix('export')->group(function () {
            Route::get('tries', 'Report\TriesReportController@export');
            Route::get('winner', 'Report\WinnerReportController@export');
            Route::get('user', 'Report\UserReportController@export');
        });
        Route::get('points', 'PointController@index');
        Route::put('points', 'PointController@update');
        Route::get('text-colors', 'TextColorController@index');
        Route::put('text-colors', 'TextColorController@update');
        Route::post('text-colors', 'TextColorController@store');

    });


    /*BRAND API*/
    Route::get('brands', 'BrandController@index');
    Route::get('brands/{id}', 'BrandController@getBy');
    Route::post('brands', 'BrandController@store');
    Route::put('brands/{id}', 'BrandController@update');
    Route::delete('brands/{id}', 'BrandController@delete');

    /*End BRAND API*/

    Route::put('settings', 'SettingController@update');

    /*Hashtag API */
   

});
