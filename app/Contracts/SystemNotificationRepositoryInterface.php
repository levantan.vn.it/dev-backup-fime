<?php

namespace App\Contracts;

/**
 * Interface BannerRepositoryInterface.
 *
 */
interface SystemNotificationRepositoryInterface extends RepositoryInterface
{
}