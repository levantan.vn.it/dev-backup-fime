<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/2/2019
 * Time: 4:35 PM
 */

namespace App\Repositories;


use App\Contracts\TipViewsRepositoryInterface;
use App\Models\TipViews;

class TipViewsRepository extends Repository implements TipViewsRepositoryInterface
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return TipViews::class;
    }
}
