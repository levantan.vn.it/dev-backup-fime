<?php

namespace App\Repositories;

use App\Contracts\UserFollowRepositoryInterface;
use App\Models\UserFollow;

/**
 * Class UserFollowRepository.
 *
 */
class UserFollowRepository extends Repository implements UserFollowRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return UserFollow::class;
    }
}
