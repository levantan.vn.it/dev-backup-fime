<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/17/2019
 * Time: 1:50 PM
 */

namespace App\Repositories;


use App\Contracts\UserRepositoryInterface;
use App\Models\User;

class UserRepository extends Repository implements UserRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return User::class;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @param int $roleId
     * @param null $searchData
     * @return mixed
     */
    public function findUsers(int $limit = 10, int $offset = 0, int $roleId = 0, $searchData = null)
    {
        // TODO: Implement findUsers() method.
        return User::withTrashed()
            ->where('role_id', $roleId)
            ->where($searchData['searchType'], 'like', '%' . $searchData['searchValue'] . '%')
            ->where('active', $searchData['isActive'] ? 1 : 0)
            ->where('allow_comment', $searchData['allowComment'] ? 1 : 0)
            ->where('allow_review', $searchData['allowReview'] ? 1 : 0)
            ->take($limit)
            ->skip($offset)
            ->get();
    }
}
