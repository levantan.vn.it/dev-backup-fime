<?php

namespace App\Repositories;

use App\Contracts\UserPointRepositoryInterface;
use App\Models\UserPoint;

/**
 * Class BannerRepository.
 *
 */
class UserPointRepository extends Repository implements UserPointRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return UserPoint::class;
    }
}
