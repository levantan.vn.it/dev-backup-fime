<?php

namespace App\Repositories;

use App\Contracts\HashtagRepositoryInterface;
use App\Models\Hashtag;

/**
 * Class HashtagRepository.
 *
 */
class HashtagRepository extends Repository implements HashtagRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Hashtag::class;
    }
}
