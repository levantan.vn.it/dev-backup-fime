<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/1/2019
 * Time: 2:09 PM
 */

namespace App\Export;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class User implements FromArray, WithEvents, WithTitle, WithCustomStartCell, WithColumnFormatting
{
    use Exportable;

    /** @var array */
    public $arr_data;

    /** @var int */
    public $startRow = 2;

    public $length = 0;

    /** @var string  */
    public $title;

    public function __construct($arr_data, string $title)
    {
        $this->arr_data = $arr_data;
        $this->length = count($arr_data);
        $this->title = $title;
    }

    /**
     * @return array
     */
    public function array(): array
    {
        // TODO: Implement array() method.
        return $this->arr_data;
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        // TODO: Implement startCell() method.
        return 'A' . ($this->startRow);
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        // TODO: Implement registerEvents() method.
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // Set heading
                $sheet = $event->sheet->getDelegate();
                $sheet->getCell('A' . ($this->startRow - 1))->setValue('User No');
                $sheet->getCell('B' . ($this->startRow - 1))->setValue('Full Name');
                $sheet->getCell('C' . ($this->startRow - 1))->setValue('Display name');
                $sheet->getCell('D' . ($this->startRow - 1))->setValue('Phone');
                $sheet->getCell('E' . ($this->startRow - 1))->setValue('Email');
                $sheet->getCell('F' . ($this->startRow - 1))->setValue('Address');
                $sheet->getCell('G' . ($this->startRow - 1))->setValue('Last Login');
                $sheet->getCell('H' . ($this->startRow - 1))->setValue('Point');
                $sheet->getCell('I' . ($this->startRow - 1))->setValue('Reviews');


                $sheet->getColumnDimension('A')->setAutoSize(true);
                $sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(30);
                $sheet->getColumnDimension('C')->setAutoSize(true);
                $sheet->getColumnDimension('D')->setAutoSize(true);
                $sheet->getColumnDimension('E')->setAutoSize(true);
                $sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(40);
                $sheet->getColumnDimension('G')->setAutoSize(true);
                $sheet->getColumnDimension('H')->setAutoSize(true);
                $sheet->getColumnDimension('I')->setAutoSize(true);
                $sheet->getStyle('A' . ($this->startRow - 1) . ':' . 'O' . ($this->startRow - 1))->getFont()->getColor()->setARGB(Color::COLOR_WHITE);

                $sheet->getStyle('A' . ($this->startRow - 1) . ':' . 'O' . ($this->startRow - 1))->getFill()->setFillType('solid')->getStartColor()->setARGB('366092');

                $horizontal_left = array(
                    'alignment' => array(
                        'horizontal' => 'left',
                    )
                );
                $horizontal_center = array(
                    'alignment' => array(
                        'horizontal' => 'center',
                    )
                );

                $sheet->getStyle('A' . ($this->startRow - 1) . ':' . 'O' . ($this->length + ($this->startRow - 1)))->applyFromArray($horizontal_left);
                $sheet->getStyle('A' . ($this->startRow - 1) . ':' . 'O' . ($this->length + ($this->startRow - 1)))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                $sheet->getStyle('C' . ($this->startRow - 1) . ':' . 'C' . ($this->length + ($this->startRow - 1)))->applyFromArray($horizontal_center);
                $sheet->getStyle('H' . ($this->startRow - 1) . ':' . 'O' . ($this->length + ($this->startRow - 1)))->applyFromArray($horizontal_center);
                $sheet->getStyle('A' . ($this->startRow - 1) . ':' . 'A' . ($this->length + ($this->startRow - 1)))->applyFromArray($horizontal_center);
            },
        ];
    }

    /**
     * @return string
     */
    public function title(): string
    {
        // TODO: Implement title() method.
        return $this->title;
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        // TODO: Implement columnFormats() method.
        return [
            'I' => NumberFormat::FORMAT_NUMBER,
            'L' => NumberFormat::FORMAT_NUMBER,
            'A' => NumberFormat::FORMAT_NUMBER,
            'C' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
