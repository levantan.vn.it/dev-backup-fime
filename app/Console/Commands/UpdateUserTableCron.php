<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class UpdateUserTableCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateUser:job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update number of reviews/followers/followings in users table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = DB::table('users')->select('id')->orderBy('updated_at', 'asc')->limit(500)->get();

        foreach ($users as $user) {
            $number_of_followings = DB::table('user_follows')->where('user_id', $user->id)->where('followed', '<>', 0)->count();
            $number_of_followers= DB::table('user_follows')->where('followed_user_id', $user->id)->where('followed', '<>', 0)->count();
            $number_of_reviews = DB::table('reviews')->where('created_by', $user->id)->whereNull('deleted_at')->count();
            $number_of_likes = DB::table('user_likes')->where('object_type', 'review')->where('user_id', $user->id)->where('is_liking', 1)->whereNull('deleted_at')->count();
            $number_of_try_free = DB::table('user_tries')->where('user_id', $user->id)->whereNull('deleted_at')->count();
            DB::table('users')->where('id', $user->id)->update([
                'number_of_reviews' => $number_of_reviews,
                'number_of_followers' => $number_of_followers,
                'number_of_followings' => $number_of_followings,
                'number_of_likes' => $number_of_likes,
                'number_of_try_free' => $number_of_try_free,
                'updated_at' => Carbon::now()
            ]);
        }


    }
}
