<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class updateReviewsHashTags extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateReviewsHashTags:job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command updateReviewsHashTags';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reviews = DB::table('TCT_REVIEW')->select('review_no', 'review_dc')->where('review_dc', 'like', '%#%')->get();
        $regex = '/#([A-Za-z-_!?.@%^&*$0-9]+)/';
        foreach ($reviews as $review) {
            $hashtags = [];
            preg_match_all($regex, $review->review_dc, $hashtags);
            if (count($hashtags) > 1) {
                $hashtags = array_map('strtoupper', array_unique($hashtags[0]));
                $existing_tags = DB::table('TCT_POPHASH')->select('hash_seq')->whereIn('hash_tag', $hashtags)->get();

                foreach ($existing_tags as $existing_tag){
                    $review_hashtag =   DB::table('review_hashtag')->where('review_no', $review->review_no)->where('hash_seq', $existing_tag->hash_seq)->count();
                    if($review_hashtag === 0){
                        DB::table('review_hashtag')->insert(['review_no' => $review->review_no, 'hash_seq' => $existing_tag->hash_seq]);
                    }
                }
            }
        }
    }
}
