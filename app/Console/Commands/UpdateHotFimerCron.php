<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class UpdateHotFimerCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateHotFimerCron:job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'calculate hot Fimers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->resetHotFimers();
        $users = $this->calculateFimers();

        foreach ($users as $user) {
            DB::table('TDM_USER')->where('user_no', $user->user_no)->update([
                'hot_fimer_7days' => (double)$user->hot_fimer_7days ?? 0,
                'hot_fimer_30days' => (double)$user->hot_fimer_30days ?? 0,
                'hot_fimer_90days' => (double)$user->hot_fimer_90days ?? 0,
                'hot_fimer_1year' => (double)$user->hot_fimer_1year ?? 0,
                'updated_at' => Carbon::now()
            ]);
        }
    }

    public function resetHotFimers()
    {
        DB::table('TDM_USER')->where('hot_fimer_7days', '<>', 0)->orWhere('hot_fimer_30days', '<>', 0)->orWhere('hot_fimer_90days', '<>', 0)->update([
            'hot_fimer_7days' => 0,
            'hot_fimer_30days' => 0,
            'hot_fimer_90days' => 0,
            'hot_fimer_1year' => 0,
            'updated_at' => Carbon::now()
        ]);
    }

    /**
     * @return mixed
     */
    public function calculateFimers()
    {
        try {
            $data = DB::select("select TDM_USER.user_no, (
	(
		select count(u.id) as total_review
		from TDM_USER u
		left join TCT_REVIEW r on u.user_no = r.user_no 
		where r.writng_dt >= DATE_SUB(now(), INTERVAL ? DAY) and TDM_USER.user_no = u.user_no
		group by u.user_no
	) * (
	(
		select (sum(total.total_like) / count(total.review_id)) as AVG_LIKE_MBR
		from TDM_USER u
		join (
			select r.review_no as review_id, r.user_no, (
				select count(ul.review_no) 
				from TCT_REVIEW_RECM ul 
				where r.review_no = ul.review_no
				and ul.recomend_dt >= DATE_SUB(now(), INTERVAL ? DAY)
				group by ul.review_no
			) as total_like
			from TCT_REVIEW r
		) as total on u.user_no = total.user_no
		where TDM_USER.user_no = u.user_no
		group by u.user_no
	) / AVG_LIKE_SVC
	)
                  ) as hot_fimer_7days, (
                    (
		select count(u.id) as total_review
		from TDM_USER u
		left join TCT_REVIEW r on u.user_no = r.user_no 
		where r.writng_dt >= DATE_SUB(now(), INTERVAL ? DAY) and TDM_USER.user_no = u.user_no
		group by u.user_no
	) * (
	(
		select (sum(total.total_like) / count(total.review_id)) as AVG_LIKE_MBR
		from TDM_USER u
		join (
			select r.review_no as review_id, r.user_no, (
				select count(ul.review_no) 
				from TCT_REVIEW_RECM ul 
				where r.review_no = ul.review_no
				and ul.recomend_dt >= DATE_SUB(now(), INTERVAL ? DAY)
				group by ul.review_no
			) as total_like
			from TCT_REVIEW r
		) as total on u.user_no = total.user_no
		where TDM_USER.user_no = u.user_no
		group by u.user_no
	) / AVG_LIKE_SVC
                        )

                  ) as hot_fimer_30days, (
                    (
		select count(u.id) as total_review
		from TDM_USER u
		left join TCT_REVIEW r on u.user_no = r.user_no 
		where r.writng_dt >= DATE_SUB(now(), INTERVAL ? DAY) and TDM_USER.user_no = u.user_no
		group by u.user_no
	) * (
	(
		select (sum(total.total_like) / count(total.review_id)) as AVG_LIKE_MBR
		from TDM_USER u
		join (
			select r.review_no as review_id, r.user_no, (
				select count(ul.review_no) 
				from TCT_REVIEW_RECM ul 
				where r.review_no = ul.review_no
				and ul.recomend_dt >= DATE_SUB(now(), INTERVAL ? DAY)
				group by ul.review_no
			) as total_like
			from TCT_REVIEW r
		) as total on u.user_no = total.user_no
		where TDM_USER.user_no = u.user_no
		group by u.user_no
	) / AVG_LIKE_SVC
                        )

                  ) as hot_fimer_90days, (
                    (
		select count(u.id) as total_review
		from TDM_USER u
		left join TCT_REVIEW r on u.user_no = r.user_no 
		where r.writng_dt >= DATE_SUB(now(),INTERVAL 1 YEAR) and TDM_USER.user_no = u.user_no
		group by u.user_no
	) * (
	(
		select (sum(total.total_like) / count(total.review_id)) as AVG_LIKE_MBR
		from TDM_USER u
		join (
			select r.review_no as review_id, r.user_no, (
				select count(ul.review_no) 
				from TCT_REVIEW_RECM ul 
				where r.review_no = ul.review_no
				and ul.recomend_dt >= DATE_SUB(now(), INTERVAL 1 YEAR)
				group by ul.review_no
			) as total_like
			from TCT_REVIEW r
		) as total on u.user_no = total.user_no
		where TDM_USER.user_no = u.user_no
		group by u.user_no
	) / (
                                  select 
                                 (select count(*) from TCT_REVIEW_RECM where recomend_dt >= DATE_SUB(now(), INTERVAL 1 YEAR)) / 
                                 (select count(*) from TCT_REVIEW where writng_dt >= DATE_SUB(now(), INTERVAL 1 YEAR))
                                 AS AVG_LIKE_SVC
                              )
                        )

                  ) as hot_fimer_1year
                    from TDM_USER 
                    join (
                      select 
                            (
                                select count(*) from TCT_REVIEW_RECM where recomend_dt >= DATE_SUB(now(), INTERVAL 90 DAY)
                            ) / 
                            (
                                select count(*) from TCT_REVIEW where writng_dt >= DATE_SUB(now(), INTERVAL 90 DAY)
                            )
                      AS AVG_LIKE_SVC
                    ) AS AVG_LIKE_SVC", [7, 7, 30, 30, 90, 90]);

            return $data;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

}
