<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Artisan;
use DB;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\UpdateUserTableCron::class,
        Commands\UpdatePicUser::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('updateHotFimerCron:job')->dailyAt('01:00');
        // for ($i = 0; $i < 2; $i++) {
        //     Artisan::call('updateTrySlugCron:job');
        //     sleep(30);
        // }
        
        // $schedule->command('UpdatePicUser:job')->everyTenMinutes();
        // $schedule->call(function () {
        //     DB::table('TDM_USER')->where('check_cron','=',0)->take(1)->update([
        //         'check_cron' =>1
        //     ]);
        // })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
