<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use App\Models\ReviewFiles;
use File;
class RenderImageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $file_name;
    protected $path;
    protected $old_name;
    public $tries = 5;
    public $timeout = 300;
    public function __construct($file_name,$path,$old_name)
    {
        $this->file_name = $file_name;
        $this->path = $path;
        $this->old_name = $old_name;
        
    
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $old_file = ReviewFiles::where('orginl_file_nm',$this->old_name)->first();
        if(empty($old_file))
            return;
        
        $file = \FFMpeg::fromDisk('videos')->open($this->path)->getFrameFromSeconds(1)->export()->toDisk('thumnails')->save($this->file_name.'.png');
        $url_image = '/data/videos/review/thumnails/'.$this->file_name.'.png';
        $type_video = 1;
        if(File::exists($url_image)){
            $size_info = getimagesize($url_image);
            $width = $size_info[0];
            $heigth = $size_info[1];
            if($width >= $heigth){
                $media = \FFMpeg::fromDisk('videos')->open($this->path)->addFilter(function ($filters) {
                        $filters->resize(new \FFMpeg\Coordinate\Dimension(640, 360));
                })->export()->inFormat(new \FFMpeg\Format\Video\X264('aac', 'libx264'))->toDisk('review')->save($this->file_name.'.mp4');
            }else{
                $type_video = 2;
                $media = \FFMpeg::fromDisk('videos')->open($this->path)->addFilter(function ($filters) {
                        $filters->resize(new \FFMpeg\Coordinate\Dimension(360, 640));
                })->export()->inFormat(new \FFMpeg\Format\Video\X264('aac', 'libx264'))->toDisk('review')->save($this->file_name.'.mp4');
            }
        }
        \FFMpeg::fromDisk('review')->open($this->file_name.'.mp4')->getFrameFromSeconds(1)->export()->toDisk('thumnails')->save($this->file_name.'.png');
        
        $check = ReviewFiles::where('orginl_file_nm', $this->old_name)->update([
            'orginl_file_nm'    =>  $this->file_name.'.mp4',
            'stre_file_nm'  =>  $this->file_name.'.mp4',
            'thumb_file_nm' =>  'thumnails/'.$this->file_name.'.png',
            'file_cours'    =>  '/data/videos/review',
            'type_file_video'    =>  $type_video
        ]);
        Storage::disk('videos')->delete($this->path);
    }
}
