<?php
namespace App\Criterias\Files;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetReviewFilesByIdsCriteria implements CriteriaInterface
{
    private $review_nos;

    public function __construct($review_nos)
    {
        $this->review_nos = $review_nos;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_REVIEW_FILE.*')
            ->where('TCT_REVIEW_FILE.file_sn', '=', 1)
            ->whereIn('TCT_REVIEW_FILE.review_no', $this->review_nos);
        return $model;
    }
}