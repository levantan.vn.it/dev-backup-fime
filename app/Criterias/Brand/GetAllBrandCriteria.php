<?php
namespace App\Criterias\Brand;

use App\Actions\Constant;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;

class GetAllBrandCriteria implements CriteriaInterface
{

    public function __construct()
    {
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('*')
            ->where('code_group', '=', Constant::$BRAND_GROUP_CODE)
            ->where('use_at', '=', 'Y')
            ->orderBy(DB::raw("TRIM(code_nm)"),'asc');
        return $model;
    }
}