<?php
namespace App\Criterias\UserLike;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetUserLikesCriteria implements CriteriaInterface
{
    private $user_id;
    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TotalLikes.id', 'TotalLikes.type', 'TotalLikes.recomend_dt')
            ->join(\DB::raw('((SELECT distinct(gr.cntnts_no) as id , u.user_no, recomend_dt, \'try\' as type 
                FROM TDM_USER u 
                INNER JOIN TCT_GOODS_RECM gr on gr.user_no = u.user_no
                INNER JOIN TOM_CNTNTS_WDTB g on g.cntnts_no = gr.cntnts_no
                WHERE g.delete_at = "N" AND g.expsr_at = "Y" 
                AND gr.user_no = \'' . $this->user_id .'\') UNION ALL
                (SELECT distinct(rr.review_no) as id , u.user_no, recomend_dt, \'review\' as type
                FROM TDM_USER u 
                INNER JOIN TCT_REVIEW_RECM rr on rr.user_no = u.user_no
                INNER JOIN TCT_REVIEW r on r.review_no = rr.review_no
                WHERE r.delete_at = "N" AND r.expsr_at = "Y" 
                AND rr.user_no = \'' . $this->user_id .'\')) AS TotalLikes'),
                function($join)
                {
                    $join->on('TDM_USER.user_no', '=', 'TotalLikes.user_no');
                })
            ->orderBy('recomend_dt', 'desc');

        return $model;
    }
}