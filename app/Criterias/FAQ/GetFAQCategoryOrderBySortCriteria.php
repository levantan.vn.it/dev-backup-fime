<?php
namespace App\Criterias\FAQ;

use App\Actions\Constant;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetFAQCategoryOrderBySortCriteria implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('*')
            ->where('code_group', '=', Constant::$FAQ_GROUP_CODE)
            ->where('use_at', '=', 'Y')
            ->orderBy('SORT_ORDR');
        return $model;
    }
}