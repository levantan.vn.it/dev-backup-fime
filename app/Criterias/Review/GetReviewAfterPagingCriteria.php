<?php

namespace App\Criterias\Review;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetReviewAfterPagingCriteria implements CriteriaInterface
{
    private $review_id;
    private $category;

    public function __construct($review_id,$category)
    {
        $this->review_id = $review_id;
        $this->category = $category;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_REVIEW.cntnts_no',
            'TCT_REVIEW.updt_dt',
            'TCT_REVIEW.delete_at',
            'TCT_REVIEW.expsr_at',
            'TCT_REVIEW.pblonsip_at',
            'TCT_REVIEW.pblonsip_snts',
            'TCT_REVIEW.pblonsip_time',
            'TCT_REVIEW.review_no',
            'TCT_REVIEW.user_no',
            'TCT_REVIEW.goods_cl_code',
            'TCT_REVIEW.goods_nm',
            'TCT_REVIEW.m_cnt',
            'TCT_REVIEW.p_cnt',
            'TCT_REVIEW.slug',
            'TCT_REVIEW.review_short',
            'TCT_REVIEW.writng_dt',
            'TCT_REVIEW.reviews_code_group',
            'TDM_USER.reg_name as author', 'TDM_USER.id as author_ds', 'TDM_USER.user_no as author_id', 'TDM_USER.pic as author_avatar', 'TDM_USER.slug as author_slug')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_REVIEW.user_no')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TCT_REVIEW.delete_at', 'N')
            ->where('TCT_REVIEW.expsr_at', 'Y')
            ->orderBy('TCT_REVIEW.review_no', 'desc');
        if ($this->review_id) {
            $model = $model->where('TCT_REVIEW.review_no', '<', $this->review_id);
        }
        if (!empty($this->category)) {
            $model = $model->where('TCT_REVIEW.goods_cl_code','=', $this->category);
            $model = $model->join('TSM_CODE', 'TSM_CODE.code', '=', 'TCT_REVIEW.goods_cl_code');
        }
        return $model;
    }
}
