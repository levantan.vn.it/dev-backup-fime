<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/19/2019
 * Time: 2:08 PM
 */

namespace App\Criterias\Review;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetNumberOfReviewsByUserIdsCriteria implements CriteriaInterface
{
    /** @var array */
    private $userIds;

    public function __construct(array $userIds = [])
    {
        $this->userIds = $userIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select(\DB::raw('count(*) as number_of_reviews'), 'user_no as user_id')
            ->where('TCT_REVIEW.delete_at', 'N')
            ->where('TCT_REVIEW.expsr_at', 'Y')
            ->whereIn('user_no', $this->userIds)
            ->groupBy('user_no');
        return $model;
    }
}
