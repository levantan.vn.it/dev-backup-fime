<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/15/2019
 * Time: 5:21 PM
 */

namespace App\Criterias\Review;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetReviewsByUserId implements CriteriaInterface
{
    /** @var int */
    private $userId;
    private $limit;

    public function __construct($userId = 1, $limit = 0)
    {
        $this->userId = $userId;
        $this->limit = $limit;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $current_user_id = auth()->id();
        $select = ['TCT_REVIEW.review_no','TCT_REVIEW.cntnts_no',
                'TCT_REVIEW.updt_dt',
                'TCT_REVIEW.delete_at',
                'TCT_REVIEW.expsr_at',
                'TCT_REVIEW.pblonsip_at',
                'TCT_REVIEW.pblonsip_snts',
                'TCT_REVIEW.pblonsip_time',
                'TCT_REVIEW.review_no',
                'TCT_REVIEW.user_no',
                'TCT_REVIEW.goods_cl_code',
                'TCT_REVIEW.goods_nm',
                'TCT_REVIEW.reviews_code_group',
                'TCT_REVIEW.m_cnt',
                'TCT_REVIEW.p_cnt',
                'TCT_REVIEW.slug',
                'TCT_REVIEW.review_short',
                'TCT_REVIEW.writng_dt',
                'TCT_REVIEW.likes as total_like',
                'TDM_USER.reg_name as author', 'TDM_USER.user_no as author_id', 'TDM_USER.pic as author_avatar', 'TDM_USER.slug as author_slug','TCT_REVIEW_FILE.file_cours','TCT_REVIEW_FILE.orginl_file_nm','TCT_REVIEW_FILE.type_file','TCT_REVIEW_FILE.thumb_file_nm'];
        $select[] = \DB::raw("
                        CASE 
                            WHEN TCT_REVIEW_FILE.type_file = 2 THEN TCT_REVIEW_FILE.stre_file_nm
                            WHEN TCT_REVIEW_FILE.thumb_file_nm is null  THEN TCT_REVIEW_FILE.stre_file_nm
                            WHEN TCT_REVIEW_FILE.thumb_file_nm = ''  THEN TCT_REVIEW_FILE.stre_file_nm                 
                            ELSE TCT_REVIEW_FILE.thumb_file_nm
                         END AS stre_file_nm");
        if(!empty($current_user_id)){
            // select is_liked = 1 if user like try or is_liked = null if user not like try
            // SELECT count(review_no) FROM TCT_REVIEW_RECM WHERE TCT_REVIEW_RECM.review_no = TCT_REVIEW.review_no AND TCT_REVIEW_RECM.user_no = '".$current_user_id."' GROUP BY TCT_REVIEW_RECM.review_no) as is_liked
            $select[] = \DB::raw("(SELECT count(review_no) FROM TCT_REVIEW_RECM WHERE TCT_REVIEW_RECM.review_no = TCT_REVIEW.review_no AND TCT_REVIEW_RECM.user_no = '".$current_user_id."' GROUP BY TCT_REVIEW_RECM.review_no) as is_liked");
        }
        $model = $model
            ->select($select)
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_REVIEW.user_no')
            ->join('TCT_REVIEW_FILE', 'TCT_REVIEW_FILE.review_no', '=', 'TCT_REVIEW.review_no')
            ->where('TCT_REVIEW.delete_at', 'N');
        if(!empty($current_user_id) && $current_user_id == $this->userId)
            $model = $model->where('TCT_REVIEW.expsr_at','!=', 'N');
        else
            $model = $model->where('TCT_REVIEW.expsr_at','=', 'Y');
        $model = $model->where('TCT_REVIEW.user_no', $this->userId)
            ->groupBy('TCT_REVIEW.review_no')
            ->orderBy('TCT_REVIEW.writng_dt', 'desc');

        if($this->limit != 0) {
            $model = $model->limit($this->limit);
        }

        return $model;
    }
}
