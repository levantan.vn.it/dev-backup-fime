<?php

namespace App\Criterias\Review;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetReviewAndAuthorPagingCriteria implements CriteriaInterface
{
    private $review_category_slug;
    private $search_value;
    private $reviewIds;
    public $category;

    public function __construct($review_category_slug = null, $search_value = null, $reviewIds = null, $category)
    {
        $this->review_category_slug = $review_category_slug;
        $this->search_value = $search_value;
        $this->reviewIds = $reviewIds;
        $this->category = $category;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        
        $category = $this->category;
        $current_user_id = auth()->id();
        $select = ['TCT_REVIEW.cntnts_no',
                'TCT_REVIEW.updt_dt',
                'TCT_REVIEW.delete_at',
                'TCT_REVIEW.expsr_at',
                'TCT_REVIEW.pblonsip_at',
                'TCT_REVIEW.pblonsip_snts',
                'TCT_REVIEW.pblonsip_time',
                'TCT_REVIEW.review_no',
                'TCT_REVIEW.user_no',
                'TCT_REVIEW.goods_cl_code',
                'TCT_REVIEW.goods_nm',
                'TCT_REVIEW.m_cnt',
                'TCT_REVIEW.p_cnt',
                'TCT_REVIEW.slug',
                'TCT_REVIEW.review_short',
                'TCT_REVIEW.writng_dt',
                'TCT_REVIEW.reviews_code_group',
                'TDM_USER.reg_name as author', 'TDM_USER.user_no as author_id',
                'TDM_USER.id as author_ds', 
                'TDM_USER.slug as author_slug',
                'TDM_USER.follower as author_follows',
                'TCT_REVIEW.likes as like_number',
                'TCT_REVIEW.comments as comment_number',
                'TCT_REVIEW_FILE.file_se_code','TCT_REVIEW_FILE.type_file','TCT_REVIEW_FILE.thumb_file_nm',
                    'TCT_REVIEW_FILE.file_cours'];
        $select[] = \DB::raw("
                        CASE 
                            WHEN TDM_USER.update_pic is null  THEN TDM_USER.pic
                            WHEN TDM_USER.update_pic = ''  THEN TDM_USER.pic                 
                            ELSE TDM_USER.update_pic
                         END AS author_avatar");
        $select[] = \DB::raw("
                        CASE 
                            WHEN TCT_REVIEW_FILE.thumb_file_nm is null  THEN TCT_REVIEW_FILE.stre_file_nm
                            WHEN TCT_REVIEW_FILE.thumb_file_nm = ''  THEN TCT_REVIEW_FILE.stre_file_nm                 
                            ELSE TCT_REVIEW_FILE.thumb_file_nm
                         END AS stre_file_nm");
        $select[] = "TCT_REVIEW_FILE.stre_file_nm as main_file";
        $select[] = "TCT_REVIEW_FILE.file_sn";
        // select 'TCT_REVIEW.cntnts_no', 'TCT_REVIEW.updt_dt', 'TCT_REVIEW.delete_at', 'TCT_REVIEW.expsr_at', 'TCT_REVIEW.pblonsip_at', 'TCT_REVIEW.pblonsip_snts', 'TCT_REVIEW.pblonsip_time', 'TCT_REVIEW.review_no', 'TCT_REVIEW.user_no', 'TCT_REVIEW.goods_cl_code', 'TCT_REVIEW.goods_nm', 'TCT_REVIEW.m_cnt', 'TCT_REVIEW.p_cnt', 'TCT_REVIEW.slug', 'TCT_REVIEW.review_short', 'TCT_REVIEW.writng_dt', 'TDM_USER.reg_name as author', 'TDM_USER.user_no as author_id', 'TDM_USER.pic as author_avatar', 'TDM_USER.id as author_ds', 'TDM_USER.slug as author_slug', 'TDM_USER.follower as author_follows', 'TCT_REVIEW.likes as like_number', 'TCT_REVIEW.comments as comment_number', 'TCT_REVIEW_FILE.file_se_code', 'TCT_REVIEW_FILE.stre_file_nm', 'TCT_REVIEW_FILE.file_cours'
        // check user login
        // $current_user_id = 'U20190626111347';
        if(!empty($current_user_id)){
            // select is_liked = 1 if user like try or is_liked = null if user not like try
            // SELECT count(review_no) FROM TCT_REVIEW_RECM WHERE TCT_REVIEW_RECM.review_no = TCT_REVIEW.review_no AND TCT_REVIEW_RECM.user_no = '".$current_user_id."' GROUP BY TCT_REVIEW_RECM.review_no) as is_liked
            $select[] = \DB::raw("(SELECT count(review_no) FROM TCT_REVIEW_RECM WHERE TCT_REVIEW_RECM.review_no = TCT_REVIEW.review_no AND TCT_REVIEW_RECM.user_no = '".$current_user_id."' GROUP BY TCT_REVIEW_RECM.review_no) as is_liked");
            // select check follow author of current user
            $select[] = \DB::raw("(SELECT count(TCT_FLLW.user_no) FROM TCT_FLLW WHERE TCT_FLLW.user_no = TCT_REVIEW.user_no AND TDM_USER.delete_at = 'N' AND TDM_USER.drmncy_at = 'N' AND TCT_FLLW.fllwr_user_no = '".$current_user_id."' GROUP BY TCT_REVIEW.user_no) as followed");
        }
        
        // if(!empty($category)){
        //     $model = $model->where('TCT_REVIEW.reviews_code_group', $category);
        //     $model = $model->join('TSM_CODE_GROUP', 'TSM_CODE_GROUP.code_group', '=', 'TCT_REVIEW.reviews_code_group');
        //     $select[] = 'TSM_CODE_GROUP.code_group_eng_nm';
        // }
        $model = $model
            ->select($select)
            //join TDM_USER on 'TDM_USER.user_no' = 'TCT_REVIEW.user_no'
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_REVIEW.user_no')
             ->leftJoin('TCT_REVIEW_FILE', 'TCT_REVIEW_FILE.review_no', '=', 'TCT_REVIEW.review_no')
            // where TDM_USER.delete_at = 'N' and TDM_USER.drmncy_at = 'N' and TCT_REVIEW.delete_at = 'N' and TCT_REVIEW.expsr_at = 'Y'
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TCT_REVIEW.delete_at', 'N')
            ->where('TCT_REVIEW.expsr_at', 'Y')
            ->groupBy('TCT_REVIEW.review_no')
            // order TCT_REVIEW.review_no desc
            ->orderBy('TCT_REVIEW.review_no', 'desc');

        //check has reviewIds to where in TCT_REVIEW.review_no $reviewIds
        if ($this->reviewIds !== null && is_array($this->reviewIds) && count($this->reviewIds)) {
            $model = $model->whereIn('TCT_REVIEW.review_no', $this->reviewIds);
        }

        // check category to join TSM_CODE on TSM_CODE.code = TCT_REVIEW.goods_cl_code
        // where TSM_CODE.slug = $this->review_category_slug
        if ($this->review_category_slug != null && $this->review_category_slug !== 'all' && $this->review_category_slug !== 'fashion' && $this->review_category_slug !== 'beauty' ) {
            $model = $model
                ->join('TSM_CODE', 'TSM_CODE.code', '=', 'TCT_REVIEW.goods_cl_code')
                ->where('TSM_CODE.slug', '=', $this->review_category_slug);
        }
        if(!empty($category)){
            $model = $model->where('TCT_REVIEW.reviews_code_group', $category);
            $model = $model->join('TSM_CODE_GROUP', 'TSM_CODE_GROUP.code_group', '=', 'TCT_REVIEW.reviews_code_group');
            $select[] = 'TSM_CODE_GROUP.code_group_eng_nm';
        }

        // check value search to where TCT_REVIEW.goods_nm like %search_value%
        // order by the nearest result
        if ($this->search_value != null) {
            $model = $model->where(function ($query) {
                $query->where("TCT_REVIEW.goods_nm", "LIKE", "%" . $this->search_value . "%");
            });

            $new = str_replace("'"," ",$this->search_value);
            $new = str_replace('"'," ",$new);
            $model = $model->orderByRaw(
                 "CASE
                    WHEN TCT_REVIEW.goods_nm LIKE '". $new ."' THEN 1
                    WHEN TCT_REVIEW.goods_nm LIKE '". $new ."%' THEN 2
                    WHEN TCT_REVIEW.goods_nm LIKE '%". $new ."' THEN 4
                    ELSE 3
                  END asc"
            );
        }
        return $model;
    }
}
