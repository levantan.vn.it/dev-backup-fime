<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/31/2019
 * Time: 2:36 PM
 */

namespace App\Criterias\TryFree;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetTryByIdCriteria implements CriteriaInterface
{
    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // TODO: Implement apply() method.
        $model = $model
            ->select('TOM_CNTNTS_FILE_CMMN.cntnts_no', 'TOM_CNTNTS_FILE_CMMN.stre_file_nm',
                'TOM_CNTNTS_FILE_CMMN.file_cours')
            ->join('TOM_CNTNTS_FILE_CMMN', 'TCT_GOODS.cntnts_no', '=', 'TOM_CNTNTS_FILE_CMMN.cntnts_no')
            ->where('TCT_GOODS.cntnts_no', '=', $this->id);

        return $model;
    }
}
