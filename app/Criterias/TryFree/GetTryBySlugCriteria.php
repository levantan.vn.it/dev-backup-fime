<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/31/2019
 * Time: 2:36 PM
 */

namespace App\Criterias\TryFree;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetTryBySlugCriteria implements CriteriaInterface
{
    private $slug;

    public function __construct($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('files.id', 'files.name', 'files.url')
            ->join('try_files', 'try_files.try_id', '=', 'tries.id')
            ->join('files', 'try_files.file_id', '=', 'files.id')
            ->where('tries.slug', '=', $this->slug);
        return $model;
    }
}
