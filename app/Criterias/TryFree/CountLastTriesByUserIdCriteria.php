<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/31/2019
 * Time: 2:36 PM
 */

namespace App\Criterias\TryFree;


use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class CountLastTriesByUserIdCriteria implements CriteriaInterface
{
    private $try_click_dt;
    public function __construct($try_click_dt)
    {
        $this->try_click_dt = $try_click_dt;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select(\DB::raw('count(TCT_GOODS.cntnts_no) as total_tries'))
            ->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->where('TOM_CNTNTS_WDTB.delete_at', 'N')
            ->where('TOM_CNTNTS_WDTB.expsr_at', 'Y');

        if($this->try_click_dt) {
            $model = $model->where('TOM_CNTNTS_WDTB.regist_dt', '>' , $this->try_click_dt);
        }

        return $model;
    }
}
