<?php

namespace App\Criterias\UserTry;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetNumberOfDeliveredByTriesIdsCriteria implements CriteriaInterface
{
    /** @var int */
    private $tryIds;

    public function __construct($tryIds)
    {
        $this->tryIds = $tryIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select(\DB::raw('count(*) as number_of_delivered'), 'cntnts_no')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_DRWT.user_no')
            ->where('TCT_DRWT.DLVY_DT', '!=',null)
            ->whereIn('cntnts_no', $this->tryIds)
            ->groupBy('cntnts_no');

        return $model;
    }
}
