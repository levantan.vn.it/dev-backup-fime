<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/19/2019
 * Time: 9:48 AM
 */

namespace App\Criterias\UserTry;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetNumberOfTriesByTryIdsCriteria implements CriteriaInterface
{
    /** @var int */
    private $tryIds;

    public function __construct($tryIds)
    {
        $this->tryIds = $tryIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select(\DB::raw('count(*) as number_of_tries'), 'cntnts_no')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_DRWT.user_no')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->whereIn('cntnts_no', $this->tryIds)
            ->groupBy('cntnts_no');

        return $model;
    }
}
