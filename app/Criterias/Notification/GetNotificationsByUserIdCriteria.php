<?php
namespace App\Criterias\Notification;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetNotificationsByUserIdCriteria implements CriteriaInterface
{
    private $user_id;
    private $page;

    public function __construct($user_id, $page)
    {
        $this->user_id = $user_id;
        $this->page = $page - 1;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $select = ['notifications.*','TCT_REVIEW.slug as review_slug','TOM_CNTNTS_WDTB.slug as try_slug','TDM_USER.slug as user_slug', 'TDM_USER.pic as user_avatar','TDM_USER.reg_name as title', DB::raw('max(notifications.id) as id'),"TOM_CNTNTS_WDTB.sj as product_name"];
        $select[] = \DB::raw("CASE
    WHEN notifications.is_system = 0 AND notifications.object_type = 'review' THEN CONCAT('/reviews/detail/',TCT_REVIEW.slug)
    WHEN notifications.is_system = 0 AND notifications.object_type = 'try_applied' THEN CONCAT('/tries/',TOM_CNTNTS_WDTB.slug)
    WHEN notifications.is_system = 0 AND notifications.object_type = 'delivery' THEN CONCAT('/tries/',TOM_CNTNTS_WDTB.slug)
    WHEN notifications.is_system = 0 AND notifications.object_type = 'user' THEN CONCAT('/usr/',TDM_USER.slug)
    ELSE ''
END as url");
        $model = $model
            ->select($select)
            ->where(function($query) {
                $query->where('notifications.user_id', '=', $this->user_id);
                $query->orWhereNull('notifications.user_id');
            })
            ->where('notifications.is_disabled', '=', 0)
            ->where('notifications.posted_at', '<=', Carbon::now())
            ->leftJoin('TCT_REVIEW', function($join){
                $join->on('notifications.object_id', '=', 'TCT_REVIEW.review_no')->where('notifications.object_type', '=', "review");
                // $join->on(DB::raw('notifications.object_type = "review"'), DB::raw(''), DB::raw(''));
            })
            ->leftJoin('TOM_CNTNTS_WDTB', function($join){
                $join->on('notifications.object_id', '=', 'TOM_CNTNTS_WDTB.cntnts_no')->where('notifications.object_type', '!=', "review");
                // $join->on(DB::raw('notifications.object_type != "review"'), DB::raw(''), DB::raw(''));
            })
            ->leftJoin("TDM_USER","notifications.reference_user_id",'=',"TDM_USER.user_no")
            ->groupBy('notifications.guid')
            ->limit(10)
            ->skip($this->page * 10)
            ->orderBy('posted_at', 'desc');
        return $model;
    }
}