<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/1/2019
 * Time: 2:00 PM
 */

namespace App\Http\Controllers\Report;


use App\Actions\ExcelExport\UserAction;
use App\Export\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\CoreRequest;
use Carbon\Carbon;
use Maatwebsite\Excel\Excel;

class UserReportController extends Controller
{
    /**
     * @param CoreRequest $userAction
     * @param UserAction $triesAction
     * @return array|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(CoreRequest $request, UserAction $userAction)
    {
        $data = $userAction->run($request->all());

        $title = 'Users';
        
        $filename = 'users_' .Carbon::now()->format('Y-m-d_His'). '.xlsx';
        (new User($data, $title))->store('/workspace/export/'.$filename, 'data');
        return $this->response(['path' => '/data/workspace/export/'.$filename]);
    }
}
