<?php

namespace App\Http\Controllers;

use App\Actions\UserFollow\GetTopInteractiveFimerAction;
use App\Actions\UserFollow\ToggleFollowUserAction;
use App\Http\Requests\UserFollow\FollowUserRequest;

class UserFollowController extends Controller
{
    /**
     * UserFollowController constructor.
     */
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['getTopInteractive']]);
    }

    /**
     * @param FollowUserRequest $request
     * @param ToggleFollowUserAction $action
     * @return array
     */
    public function toggle(FollowUserRequest $request, ToggleFollowUserAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    public function getTopInteractive(GetTopInteractiveFimerAction $action) {
        $data = $action->run();
        return $this->response($data);
    }
}
