<?php

namespace App\Http\Controllers\Auth;

use App\Actions\Auth\ForgotPassword;
use App\Actions\Auth\Login;
use App\Actions\Auth\LoginByFacebook;
use App\Actions\Auth\Logout;
use App\Actions\Auth\Register;
use App\Actions\Auth\ResetPassword;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ForgotPasswordRequest;
use App\Http\Requests\Auth\LoginByFacebookRequest;
use App\Http\Requests\Auth\LoginUserRequest;
use App\Http\Requests\Auth\RegisterUserRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Http\Requests\CoreRequest;
use App\Contracts\UserRepositoryInterface;
use App\Http\Requests\Auth\ChangePasswordRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
class AuthController extends Controller
{
    private $userRepository;
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->middleware('jwt.auth', ['except' => ['login', 'register', 'loginByFacebook', 'resetPassword', 'forgotPassword','verifyEmail','resendVerifyEmail']]);
    }
    /**
     * @SWG\Post(
     *      path="/auth/login",
     *      operationId="login",
     *      tags={"Auth"},
     *      summary="Login",
     *      description="Login",
     *      @SWG\Parameter(
     *          name="email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     *
     * @param LoginUserRequest $request
     * @param Login $action
     * @return array
     */
    public function login(LoginUserRequest $request, Login $action)
    {
        $data = $action->run($request->all(['email', 'password']));
        return $this->response($data);
    }

    /**
     * @SWG\Post(
     *      path="/auth/loginByFacebook",
     *      operationId="loginByFacebook",
     *      tags={"Auth"},
     *      summary="Login By Facebook",
     *      description="Login By Facebook",
     *      @SWG\Parameter(
     *          name="accessToken",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="userID",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     *
     * @param LoginByFacebookRequest $request
     * @param LoginByFacebook $action
     * @return array
     */
    public function loginByFacebook(LoginByFacebookRequest $request, LoginByFacebook $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param CoreRequest $request
     * @param Logout $action
     * @return array
     */
    public function logout(CoreRequest $request, Logout $action)
    {
        $data = $action->run();
        return $this->response($data);
    }

    /**
     * @SWG\Post(
     *      path="/auth/resetPassword",
     *      operationId="resetPassword",
     *      tags={"Auth"},
     *      summary="Reset password",
     *      description="Reset password",
     *      @SWG\Parameter(
     *          name="token",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     *
     * @param ResetPasswordRequest $request
     * @param ResetPassword $action
     * @return array
     */
    public function resetPassword(ResetPasswordRequest $request, ResetPassword $action)
    {
        $data = $action->run($request);
        return $this->response($data);
    }

    /**
     *  @SWG\Post(
     *      path="/auth/forgotPassword",
     *      operationId="forgotPassword",
     *      tags={"Auth"},
     *      summary="Forgot password",
     *      description="Forgot password",
     *      @SWG\Parameter(
     *          name="email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     * @param ForgotPasswordRequest $request
     * @param ForgotPassword $action
     * @return array
     */
    public function forgotPassword(ForgotPasswordRequest $request, ForgotPassword $action)
    {
        $data = $action->run($request);
        return $this->response($data);
    }

    /**
     * @SWG\Post(
     *      path="/auth/register",
     *      operationId="register",
     *      tags={"Auth"},
     *      summary="Register User",
     *      description="Register User",
     *      @SWG\Parameter(
     *          name="email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
 *          @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     * @param RegisterUserRequest $request
     * @param Register $action
     * @return array
     */
    public function register(RegisterUserRequest $request, Register $action)
    {
        $data = $action->run($request);
        return $this->response($data);
    }


    public function changePassword(ChangePasswordRequest $request)
    {
        $user = $this->userRepository->find($request->id);
        $newPassword = Hash::make($request['password']);
        if (!password_verify($request['curpassword'], $user->password)) {
            return $this->response([ 'error' => 'Mật khẩu cũ không chính xác. Vui lòng kiểm tra lại']);
        }
        $this->userRepository->update(['password' => $newPassword], $request->id);
        return $this->response(['done' => 1]);
    }

    public function verifyEmail(Request $request)
    {

        if(empty($request->user_no)){
            return $this->response(['error'=>2]);
        }
        if(empty($request->access_token)){
            return $this->response(['error'=>2]);
        }
        $user = User::where('user_no',$request->user_no)->where('access_token',$request->access_token)->first();
        
        if(empty($user)){
            return $this->response(['error'=>2]);
        }
        if(!empty($user->verification)){
            return $this->response(['error'=>3]);
        }
        $verification_max = $user->verification_max;
        if(empty($verification_max)){
            return $this->response(['error'=>2]);
        }
        if (strtotime($verification_max) + 86400 < time()){
            return $this->response(['error'=>1]);
        }
        $user->verification = 1;
        $user->verification_at = date('Y-m-d H:i:s');
        $user->access_token = "";
        $user->verification_max = "";
        $user->save();
        return $this->response(['success'=>true]);
        
    }

    public function resendVerifyEmail(Request $request)
    {

        if(empty($request->user_no)){
            return $this->response(['error'=>2]);
        }
        
        $user = User::where('user_no',$request->user_no)->first();
        
        if(empty($user)){
            return $this->response(['error'=>2]);
        }
        if(!empty($user->verification)){
            return $this->response(['error'=>1]);
        }
        $verification_max = $user->verification_max;
        if(empty($verification_max)){
            return $this->response(['error'=>2]);
        }
        if (strtotime($verification_max) + 86400 < time()){
            return $this->response(['error'=>1]);
        }
        $access_token = str_random(10);
        $user->verification_max = date('Y-m-d H:i:s');
        $user->access_token = $access_token;
        $user->save();

        $to_name = $user->reg_name;
        $to_email = $user->email;
        $data = array(
            'user_no'   =>  $user_no,
            'access_token'  =>  $access_token,
            'url'   =>  'http://13.228.141.200/verify/'.$user_no.'/'.$access_token
        );
        \Mail::send('emails.verification', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)->subject("Email Xác thực(Verify Email)");
        });
        return $this->response(['success'=>true]);
        
    }
}
