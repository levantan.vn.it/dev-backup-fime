<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/5/2019
 * Time: 3:59 PM
 */

namespace App\Http\Controllers;


use App\Actions\Point\GetAllAction;
use App\Actions\Point\UpdateAction;
use App\Http\Requests\CoreRequest;

class PointController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['index']]);
    }

    public function index(CoreRequest $coreRequest, GetAllAction $action){
        return $this->response($action->run());
    }

    public function update(CoreRequest $request, UpdateAction $action) {
        return $this->response($action->run($request->all()));

    }
}
