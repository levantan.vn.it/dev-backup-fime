<?php

namespace App\Http\Controllers;

use App\Actions\Role\GetAllRoles;
use App\Http\Requests\CoreRequest;

class RoleController extends Controller
{
    public function getList(CoreRequest $request, GetAllRoles $action){
        return $this->response($action->run($request));
    }
}
