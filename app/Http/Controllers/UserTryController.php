<?php

namespace App\Http\Controllers;

use App\Actions\UserTry\ApplyTryAction;
use App\Actions\UserTry\GetShippingsAction;
use App\Actions\UserTry\GetShippingStatusesAction;
use App\Actions\UserTry\GetUserTriesAction;
use App\Actions\UserTry\ToggleUserTryAction;
use App\Actions\UserTry\UpdateDeliveryInfoAction;
use App\Http\Requests\CoreRequest;
use App\Http\Requests\UserTry\ApplyTryRequest;
use App\Http\Requests\UserTry\GetUserTriesRequest;
use App\Http\Requests\UserTry\ToggleUserTryRequest;

class UserTryController extends Controller
{
    /**
     * UserFollowController constructor.
     */
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['']]);
    }

    public function getAll(GetUserTriesRequest $request, GetUserTriesAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
    /**
     * @SWG\Post(
     *      path="/user-tries",
     *      operationId="apply",
     *      tags={"User Try"},
     *      summary="Apply Try",
     *      description="Apply Try",
     *      @SWG\Parameter(
     *          name="try_id",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"jwt": {}}
     *       }
     *     )
     *
     * @param ApplyTryRequest $request
     * @param ApplyTryAction $action
     * @return array
     */
    public function apply(ApplyTryRequest $request, ApplyTryAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    public function updateDeliveryInfo(CoreRequest $request, UpdateDeliveryInfoAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    public function toggle(ToggleUserTryRequest $request, ToggleUserTryAction $action) {
        $data = $action->run($request->id, $request->user_no);
        return $this->response($data);
    }

    public function getShippings(GetShippingsAction $action) {
        $data = $action->run();
        return $this->response($data);
    }

    public function getShippingStatuses(GetShippingStatusesAction $action) {
        $data = $action->run();
        return $this->response($data);
    }
}
