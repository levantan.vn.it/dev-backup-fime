<?php

namespace App\Actions\Tip;

use App\Actions\Action;
use App\Actions\Constant;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Repositories\ReviewRepository;
use Carbon\Carbon;

class GetReviewsTipMapping extends Action
{
    protected $review_repository;
    private $review_files_repository;

    public function __construct(ReviewRepository $review_repository,
                                ReviewFilesRepositoryInterface $review_files_repository)
    {
        $this->review_repository = $review_repository;
        $this->review_files_repository = $review_files_repository;
    }

    public function run($id) {
        $result = $this->review_repository->scopeQuery(function($query) use ($id){
            $query = $query->select(
            'TCT_TIPS_MAPPING.content_id',
            'TCT_REVIEW.REVIEW_NO',
            'TCT_REVIEW.review_short',
            'TCT_REVIEW.WRITNG_DT',
            'TCT_REVIEW.GOODS_NM',
            'TCT_REVIEW.GOODS_NM',
            'TDM_USER.reg_name')
            ->leftJoin('TCT_TIPS_MAPPING','TCT_TIPS_MAPPING.content_id','=','TCT_REVIEW.review_no')
            ->leftJoin('TCT_TIPS','TCT_TIPS.ti_id','=','TCT_TIPS_MAPPING.ti_id')
            ->leftJoin('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_REVIEW.user_no');
            return $query->where('TCT_TIPS_MAPPING.content_gb','=', 'R')
            ->where('TCT_TIPS.ti_id', $id);
        })->get();
       return $result;
    }
}

