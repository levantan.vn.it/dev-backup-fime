<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Tip;
use App\Contracts\TipsRepositoryInterface;
use App\Contracts\TipsMappingRepositoryInterface;
use App\Actions\Action;
use Mockery\Exception;

class DeleteTipAction extends Action {
    protected $repository;
    protected $tipsMappingRepository;

    public function __construct(TipsRepositoryInterface $repository,
    TipsMappingRepositoryInterface $tipsMappingRepository) {
        $this->repository = $repository;
        $this->tipsMappingRepository = $tipsMappingRepository;
    }

    public function run($ids) {
        try {
            
            foreach ($ids as $id) {
                $this->repository->delete($id);
            }
            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
