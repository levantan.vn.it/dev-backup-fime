<?php

namespace App\Actions\Tip;

use App\Actions\Action;
use App\Actions\Constant;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Repositories\ReviewRepository;
use Carbon\Carbon;

class GetListReviewAction extends Action
{
    protected $review_repository;
    private $review_files_repository;

    public function __construct(ReviewRepository $review_repository,
                                ReviewFilesRepositoryInterface $review_files_repository)
    {
        $this->review_repository = $review_repository;
        $this->review_files_repository = $review_files_repository;
    }

    public function run($params)
    {
        $reviews = $this->review_repository->scopeQuery(function ($query) use ($params) {
            $query = $query->leftJoin('TSM_CODE', 'TSM_CODE.code', '=', 'TCT_REVIEW.goods_cl_code')
                ->leftJoin('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_REVIEW.user_no')
                ->leftJoin('TCT_MAIN_CONTS', function ($join) {
                    $join->on('TCT_MAIN_CONTS.ref_no', '=', 'TCT_REVIEW.review_no')
                        ->where('TCT_MAIN_CONTS.cont_type', Constant::$POPULAR_CONT_TYPE_CODE)
                        ->where('TCT_MAIN_CONTS.cont_std', Constant::$POPULAR_CONT_STD_CODE);
                })
                ->where('TCT_REVIEW.delete_at', 'N')
                ->where('TCT_REVIEW.expsr_at', 'Y');

            if (isset($params['name']) && $params['name'] != "null") {
                $query = $query->where('goods_nm', 'like', '%' . $params['name'] . '%');
            }
            if (isset($params['reg_name']) && $params['reg_name'] != "null") {
                $query = $query->where('TDM_USER.reg_name', 'like', '%' . $params['reg_name'] . '%');
            }

            return $query->select('TCT_REVIEW.cntnts_no',
                'TCT_REVIEW.writng_dt',
                'TCT_REVIEW.delete_at',
                'TCT_REVIEW.expsr_at',
                'TCT_REVIEW.review_no',
                'TCT_REVIEW.review_short',
                'TCT_REVIEW.user_no',
                'TCT_REVIEW.goods_cl_code',
                'TCT_REVIEW.goods_nm',
                'TCT_REVIEW.slug',
                'TCT_REVIEW.view_cnt',
                'TDM_USER.reg_name', 'TSM_CODE.code_nm', 'TCT_MAIN_CONTS.conts_seq')
                ->orderBy($params['column'], $params['sort'])->orderBy('TCT_REVIEW.writng_dt', 'desc');
        })->paginate($params['pageSize']);


        $data = $reviews->toArray();

        // $data['data'] = $reviewsData;
        return $data;
    }
}

