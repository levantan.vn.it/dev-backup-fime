<?php

namespace App\Actions\Tip;
use App\Actions\Action;
use App\Contracts\TryRepositoryInterface;
use App\Models\TryFree;


class GetTryByIdAction extends Action {
    protected $repository;
    public function __construct(TryRepositoryInterface $repository) {
        $this->repository = $repository;

    }

    public function run($id) {
        try {
            $try = TryFree::join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->leftJoin('TOM_CNTNTS_FILE_CMMN', 'TOM_CNTNTS_FILE_CMMN.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->find($id,['TCT_GOODS.cntnts_no','TOM_CNTNTS_WDTB.sj','TCT_GOODS.event_bgnde','TCT_GOODS.event_endde','TOM_CNTNTS_FILE_CMMN.*']);

            return $try;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }
}
