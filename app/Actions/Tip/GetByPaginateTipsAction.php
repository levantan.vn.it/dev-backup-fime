<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Tip;

use App\Actions\Action;
use App\Contracts\TipsRepositoryInterface;
use Carbon\Carbon;

class GetByPaginateTipsAction extends Action {
    protected $repository;

    public function __construct(TipsRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($params) {
        
        $tips = $this->repository->scopeQuery(function($query) use ($params){
            $query = $query->leftJoin('tip_views', 'tip_views.ti_id', '=', 'TCT_TIPS.ti_id');
            $query = $query->leftJoin('TSM_CODE','TSM_CODE.code','=','TCT_TIPS.tips_cl_code');

            if(isset($params['name']) && $params['name'] != "null") {
                $query = $query->where('subject', 'like', '%' . $params['name'] . '%');
            }

            if(isset($params['display_yn']) && $params['display_yn'] != "null") {
                $query = $query->where('display_yn', '=', $params['display_yn']);
            }

            if(isset($params['main_open_yn']) && $params['main_open_yn'] != "null") {
                $query = $query->where('main_open_yn', '=', $params['main_open_yn']);
            }

            $query = $query->select('TCT_TIPS.ti_id',
            'TCT_TIPS.subject',
            'TCT_TIPS.img_url',
            'TCT_TIPS.cover_img1',
            'TCT_TIPS.created_at',
            'TCT_TIPS.updated_at',
            'TCT_TIPS.display_yn',
            'tip_views.views',
            'TCT_TIPS.main_open_yn',
            'TSM_CODE.code_nm');
            return $query->orderBy('TCT_TIPS.ti_id', 'desc');
        })->paginate($params['pageSize']);
        // dd($tips);
       return $tips;
    }
}
