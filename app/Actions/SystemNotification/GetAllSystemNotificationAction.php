<?php

namespace App\Actions\SystemNotification;
use App\Actions\Action;
use App\Contracts\SystemNotificationRepositoryInterface;

class GetAllSystemNotificationAction extends Action {
    protected $system_notification_repository;

    public function __construct(SystemNotificationRepositoryInterface $system_notification_repository) {
        $this->system_notification_repository = $system_notification_repository;
    }

    public function run($params) {
    	if(!empty($params['pageSize'])){
    	    $ads = $this->system_notification_repository->scopeQuery(function($query) use ($params){
    	        $query = $query->where('is_system', 1);
    	        $query = $query->where('user_id', '=', null);
    	        return $query->orderBy('created_at', 'desc');
    	    })->paginate($params['pageSize']);
    	}else{
    	   $ads = $this->system_notification_repository->orderBy('created_at', 'desc')->findWhere(['is_system' => 1, 'user_id' => null]);
    	}
        
        return $ads;
    }
}
