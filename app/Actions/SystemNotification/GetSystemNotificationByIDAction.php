<?php

namespace App\Actions\SystemNotification;
use App\Actions\Action;
use App\Contracts\SystemNotificationRepositoryInterface;

class GetSystemNotificationByIDAction extends Action {
    protected $system_notification_repository;

    public function __construct(SystemNotificationRepositoryInterface $system_notification_repository) {
        $this->system_notification_repository = $system_notification_repository;
    }

    public function run($id) {
        try {
            $data = $this->system_notification_repository->find($id);
            return $data;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }
}
