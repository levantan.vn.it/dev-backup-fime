<?php

namespace App\Actions\SystemNotification;

use App\Actions\Action;
use App\Contracts\SystemNotificationRepositoryInterface;
use Mockery\Exception;

class ToggleSystemNotificationAction extends Action {
    protected $repository;

    public function __construct(SystemNotificationRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($ids, $toggle) {
        try {
            $notifications = $this->repository->findWhereIn('id', $ids);

            foreach ($notifications as $notification) {
                $this->repository->update([
                    'is_disabled' => $toggle
                ], $notification->id);
            }
            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
