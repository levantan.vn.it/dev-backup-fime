<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/5/2019
 * Time: 4:02 PM
 */

namespace App\Actions\Point;


use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;
use Mockery\Exception;

class UpdateAction extends Action
{
    private $codeRepository;

    public function __construct(CodeRepositoryInterface $codeRepository)
    {
        $this->codeRepository = $codeRepository;
    }

    public function run($request)
    {
        try {
            $codes = $request['data'];
            foreach ($codes as $code) {
                $this->codeRepository->update([
                    'refrn_info' => $code['refrn_info']
                ], $code['code']);
            }
            return $this->codeRepository->findByField('code_group', 405);
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
