<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Categories;

use App\Actions\Action;
use App\Contracts\CodeGroupRepositoryInterface;
use Mockery\Exception;

class DeleteCategoriesAction extends Action {
    protected $repository;

    public function __construct(CodeGroupRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($id) {
        try {
            $this->repository->update([
                'use_at' => 'N'
            ], $id);
            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
