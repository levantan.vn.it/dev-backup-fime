<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Categories;

use App\Actions\Action;
use App\Contracts\CodeGroupRepositoryInterface;
use Mockery\Exception;
use Carbon\Carbon;


class AddCategoriesAction extends Action {
    protected $repository;

    public function __construct(CodeGroupRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($data) {
        try {
            $last_item = $this->repository->orderBy('CODE_GROUP', 'desc')->first();
            $code = $last_item->code_group + 1;
            $register_date = Carbon::now('Asia/Ho_Chi_Minh');

            $category = $this->repository->create([
                'code_group' => $code,
                'code_group_eng_nm' => $data['code_group_eng_nm'],
                'code_group_nm' => $data['code_group_eng_nm'],
                'use_at' => 'Y',
                'register_esntl_no' => 'MB00000014',
                'regist_dt' => $register_date->toDateString(),
                'updusr_esntl_no' => 'MB00000014',
                'updt_dt' => $register_date->toDateString()
            ]);
            return $category;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
