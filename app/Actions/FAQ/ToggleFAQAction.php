<?php

namespace App\Actions\FAQ;
use App\Actions\Action;
use App\Contracts\TryExtRepositoryInterface;
use Mockery\Exception;

class ToggleFAQAction extends Action {
    protected $faq_repository;

    public function __construct(TryExtRepositoryInterface $faq_repository) {
        $this->faq_repository = $faq_repository;
    }

    public function run($cntnts_nos, $toggle) {
        try {
            $faqs = $this->faq_repository->findWhereIn('cntnts_no', $cntnts_nos);

            foreach ($faqs as $faq) {
                $this->faq_repository->update([
                    'expsr_at' => $toggle
                ], $faq->cntnts_no);
            }

            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
