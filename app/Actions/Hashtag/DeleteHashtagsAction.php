<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Hashtag;
use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\HashtagRepositoryInterface;
use Mockery\Exception;

class DeleteHashtagsAction extends Action {
    protected $repository;

    public function __construct(HashtagRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($id) {
        try {
            $hashtag = $this->repository->delete($id);
            return $hashtag;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
