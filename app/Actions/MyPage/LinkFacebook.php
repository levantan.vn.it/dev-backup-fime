<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/20/2019
 * Time: 3:17 PM
 */

namespace App\Actions\MyPage;


use App\Actions\Action;
use App\Contracts\UserRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class LinkFacebook extends Action
{
    protected $userRepository;

    public function __construct(
        UserRepositoryInterface $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    public function run($data)
    {
        $token = $data['token'];

        if ($token != '') {
            $is_used_fb = $this->userRepository->findWhere(['sns_id' => $token])->first();

            if ($is_used_fb) {
                return ['error' => 'This facebook account is used.'];
            }
        }

        $rs = $this->userRepository->update([
            'sns_id' => $token
        ], Auth::user()->user_no);

        return $rs;
    }
}
