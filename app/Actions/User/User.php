<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/17/2019
 * Time: 2:01 PM
 */

namespace App\Actions\User;


use App\Actions\Action;
use App\Contracts\PasswordResetRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;

class User extends Action
{
    protected $userRepository;
    private $passwordResetRepository;

    public function __construct(UserRepositoryInterface $userRepository,
                                PasswordResetRepositoryInterface $passwordResetRepository)
    {
        $this->userRepository = $userRepository;
        $this->passwordResetRepository = $passwordResetRepository;
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function getUserById(string $id)
    {
        return $this->userRepository->find($id);
    }

    /**
     * @param string $email
     * @return mixed
     */
    public function getUserByEmail(string $email)
    {
        return $this->userRepository->findByField('email', $email);
    }

    /**
     * @param string $user_no
     * @return mixed
     */
    public function deleteUser(string $user_no)
    {
        try {
            $user = $this->userRepository->update([
                'delete_at' => 'Y',
                'deleted_at' => Carbon::now()
            ], $user_no);
            return $user;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function updateStatus(array $data)
    {
        try {
            $user = $this->userRepository->update([
                'drmncy_at' => (int)$data['active'] === 1 ? 'N' : 'Y',
                'allow_comment' => (int)$data['allow_comment'],
                'allow_review' => (int)$data['allow_review']
            ], $data['user_no']);
            return $user;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function update(array $data)
    {
        try {
            if (isset($data['deleted'])) {
                if ((int)$data['deleted'] === 1) {
                    $this->userRepository->update([
                        'delete_at' => 'Y',
                        'deleted_at' => Carbon::now()
                    ], $data['user_no']);
                }
            }
            $user = $this->userRepository->update([
                'drmncy_at' => (int)$data['active'] === 1 ? 'N' : 'Y',
                'allow_comment' => (int)$data['allow_comment'],
                'allow_review' => (int)$data['allow_review'],
                'home_addr1' => (string)$data['home_addr1'],
                'cellphone' => (string)$data['cellphone'],
                'email' => (string)$data['email'],
                'reg_name' => (string)$data['reg_name'],
                'id' => (string)$data['id'],
                'role_id' => (int)$data['role_id'],
                'updated_at' => Carbon::now()
            ], $data['user_no']);

            if ($data['role_id'] == 1) {
                $user->assignRole('admin');
            } else {
                $user->removeRole('admin');
            }

            return $user;

        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    public function add(array $data)
    {
        try {
            $is_email_existed = $this->userRepository->findWhere(['email' => $data['email']]);
            if (count($is_email_existed) > 0) {
                return [
                    'error' => 1
                ];
            }
            $is_display_name_existed = $this->userRepository->findWhere(['id' => $data['id']]);
            if (count($is_display_name_existed) > 0) {
                return [
                    'error' => 2
                ];
            }
            $data['password'] = 123456; //default value
            $data['user_no'] = 'U' . date_format(Carbon::now(), 'YmdHis'); //default value
            $data['drmncy_at'] = (int)$data['active'] === 1 ? 'N' : 'Y';
            $new_user = $this->userRepository->create($data);

            if ($data['role_id'] == 1) {
                $new_user->assignRole('admin');
            } else {
                $new_user->removeRole('admin');
            }
            $user = $this->userRepository->findWhere(['email' => $data['email']]);
            if (count($user)) {
                /**
                 * Create a new token to be sent to the user, save it into database
                 */
                $email = $data['email'];
                $token = str_random(60);
                $created_at = Carbon::now();
                $expired_at = $created_at->addDay(3);
                $data = ['email' => $email, 'token' => $token, 'created_at' => $created_at, 'expired_at' => $expired_at];
                $last_token = $this->passwordResetRepository->findWhere(['email' => $data['email']]);
                if (count($last_token) == 0) {
                    $this->passwordResetRepository->create($data);
                } else {
                    $last_token = $last_token[0];
                    $this->passwordResetRepository->update($data, $last_token->id);
                }

                /**
                 * Send email to the email above with a link to your password reset
                 */
                $url = env('APP_UI_URL', 'https://web-np.fime.vn') . '/reset-password/' . $token;
                $forgot_password_url = env('APP_UI_URL', 'localhost:4200') . '/forgot-password/';
                Mail::send('resetPasswordMail', ['url' => $url, 'forgot_password_url' => $forgot_password_url], function ($message) use ($user, $email) {
                    $to = $email;
                    $message->to($email, $user[0]->name)->subject('Set your fi:me account password');
                });
            }

            return $new_user;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
