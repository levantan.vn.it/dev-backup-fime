<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\UserLike;
use App\Actions\Action;
use App\Contracts\ReviewLikeRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Criterias\UserFollow\GetFollowingOfCurrentUserCriteria;
use App\Criterias\UserLike\GetListUserLikeInfoCriteria;
use App\Criterias\UserLike\GetReviewLikeInfoCriteria;
use App\Repositories\UserLikeRepository;

class GetListUserLiked extends Action {
    protected $user_like_repository;
    protected $followRepository;
    protected $user_like_review_repository;

    public function __construct(UserLikeRepository $user_like_repository,
                                ReviewLikeRepositoryInterface $user_like_review_repository,
                                UserFollowRepositoryInterface $followRepository) {
        $this->user_like_repository = $user_like_repository;
        $this->followRepository = $followRepository;
        $this->user_like_review_repository = $user_like_review_repository;
    }

    public function run($data) {
        $object_id = $data['object_id'];
        $object_type = $data['object_type'];
        $fimers = [];
        if ($object_type == 'try') {
            $list_user_liked = $this->user_like_repository->pushCriteria(new GetListUserLikeInfoCriteria($object_id))->paginate(12);
            $fimers = $this->getCurrentUserFollowing($list_user_liked);
        } else if ($object_type == 'review') {
            $list_user_liked = $this->user_like_review_repository->pushCriteria(new GetReviewLikeInfoCriteria($object_id))->paginate(12);
            $fimers = $this->getCurrentUserFollowing($list_user_liked);
        }
        return $fimers;
    }

        protected function getCurrentUserFollowing($fimers)
    {
        $current_user_id = auth()->id();

        $user_ids = $fimers->pluck('user_no')->toArray();

        $user_followings = $this->followRepository->getByCriteria(
            new GetFollowingOfCurrentUserCriteria($user_ids, $current_user_id))->keyBy('followed_user_id');

        foreach ($fimers as $fimer) {
            $fimer->followed = 0;

            if (isset($user_followings[$fimer->user_no])) {
                $fimer->followed = 1;
            }
        }
        return $fimers;
    }
}
