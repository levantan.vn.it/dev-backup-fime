<?php

namespace App\Actions;

/**
 * Class Action.
 *
 */
class Constant
{
    public static $POINT_GROUP_CODE = 405;
    public static $BRAND_GROUP_CODE = 406;
    public static $CATEGORY_GROUP_CODE = 407;
    public static $SHIPPING_GROUP_CODE = 408;
    public static $SHIPPING_STATUS_GROUP_CODE = 409;
    public static $FAQ_GROUP_CODE = 402;
    public static $POPULAR_CONT_STD_CODE = '501001';
    public static $POPULAR_CONT_TYPE_CODE = '500002';
    public static $FILE_SE_CODE = '185002';
    public static $MAIN_SE_CODE = '209004';
    public static $OTHER_SE_CODE = '209003';
    public static $DESC_SE_CODE = '209005';
    public static $JOIN_CODE = '405001';
    public static $JOIN_POINT = 20;
    public static $TRY_APPLIED_CODE = '405003';
    public static $TRY_APPLIED_POINT = 5;
    public static $WRITE_REVIEW_CODE = '405009';
    public static $WRITE_REVIEW_POINT = 10;
    public static $COMMENT_REVIEW_CODE = '405006';
    public static $COMMENT_REVIEW_POINT = 2;
    public static $LIKE_REVIEW_CODE = '405008';
    public static $LIKE_TRY_CODE = '405007';
    public static $LIKE_POINT = 1;
    public static $TRY_WINNER_CODE = '405004';
    public static $TRY_REVIEW_WINNER_CODE = '405005';
}
