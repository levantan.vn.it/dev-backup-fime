<?php

namespace App\Actions\Notification;

use App\Actions\Action;
use App\Contracts\NotificationRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Criterias\Notification\GetNotificationNumberByUserIdCriteria;
use App\Models\Notification;
class GetNotificationNumberByUserIdAction extends Action
{
    protected $notification_repository;
    protected $review_repository;

    public function __construct()
    {
    }

    public function run()
    {

        $current_user_id = auth()->id();
        $model = Notification::select('user_id')->where('notifications.is_seen', '=', 0)->where('notifications.user_id', '=', $current_user_id)->where('notifications.is_disabled',0)->whereNull('notifications.deleted_at')->groupBy('user_id')->count();
        return $model;
    }
}
