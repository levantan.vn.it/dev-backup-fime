<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/25/2019
 * Time: 5:49 PM
 */

namespace App\Actions\Role;


use App\Actions\Action;
use App\Contracts\RoleRepositoryInterface;

class GetAllRoles extends Action
{
    protected $repository;

    public function __construct(RoleRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run() {
        $roles = $this->repository->all();
        return $roles;
    }
}
