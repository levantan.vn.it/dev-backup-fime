<?php


namespace App\Actions\Users;


use App\Actions\Action;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Contracts\RoleRepositoryInterface;
use App\Criterias\Users\GetListUsersCriteria;
use App\Contracts\UserPointRepositoryInterface;
use App\Criterias\Users\GetListHotFimersCriteria;

class GetListHotFimersAction extends Action
{

    protected $userRepository;
    protected $roleRepository;
    protected $reviewRepository;
    protected $followRepository;
    protected $reviewFilesRepository;
    protected $userPointRepository;
    public function __construct(
        UserRepositoryInterface $userRepository,
        UserFollowRepositoryInterface $followRepository,
        ReviewRepositoryInterface $reviewRepository,
        RoleRepositoryInterface $roleRepository,
        ReviewFilesRepositoryInterface $reviewFilesRepository,
        UserPointRepositoryInterface $userPointRepository
    ) {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->reviewRepository = $reviewRepository;
        $this->followRepository = $followRepository;
        $this->userPointRepository = $userPointRepository;
        $this->reviewFilesRepository = $reviewFilesRepository;
    }

    public function run($request)
    {

        try {

            $searchType = isset($request['searchType']) ? $request['searchType'] : '';
            $searchValue = isset($request['searchValue']) ? $request['searchValue'] : '';
            $allowComment = isset($request['allowComment']) ? $request['allowComment'] : 1;
            $allowReview = isset($request['allowReview']) ? $request['allowReview'] : 1;
            $isActive = isset($request['isActive']) ? ($request['isActive'] === 'true' ? 'N' : 'Y') : 'N';
            $isDelete = isset($request['isDelete']) ? ($request['isDelete'] === 'true' ? 'Y' : 'N') : 'N';
            $role = isset($request['role']) ? $request['role'] : 'fimer';

            $role_id = $this->roleRepository->findByField('name', $role)->first();

            $fimers = $this->userRepository->pushCriteria(new GetListHotFimersCriteria($searchType, $searchValue, $allowComment, $allowReview, $isActive, $isDelete, $role_id->id))->paginate(10);
            $this->getUsersPoint($fimers);

            return $fimers;
        } catch (\Exception $e) {
            \Log::error($e);
        }
        return [];
       
    }

    public function getUsersPoint($users){
        $user_ids = $users->pluck('user_no')->toArray();

        $points = $this->userPointRepository->scopeQuery(function ($query) use ($user_ids){
            $query = $query->select(\DB::raw('sum(accml_pntt) as total_point'), 'user_no');
            $query = $query->join('TSM_CODE', 'TCT_PNTT.pntt_accml_code', '=', 'TSM_CODE.code');
            $query = $query->whereIn('user_no', $user_ids);
            $query = $query->where('TSM_CODE.code_group', 405);
            $query = $query->where('TSM_CODE.use_at', 'Y');
            $query = $query->orderBy('total_point', 'desc');
            $query = $query->groupBy('user_no');
            return $query;
        });

        $points = $points->all()->keyBy('user_no');
        foreach ($users as $user) {
            if (isset($points[$user->user_no])) {
                $user->total_point = $points[$user->user_no]->total_point;
            } else {
                $user->total_point = 0;
            }
        }
    }
}
