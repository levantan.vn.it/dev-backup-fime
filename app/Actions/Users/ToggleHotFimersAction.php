<?php

namespace App\Actions\Users;
use App\Actions\Action;
use App\Repositories\UserRepository;
use Mockery\Exception;

class ToggleHotFimersAction extends Action {
    protected $user_repository;

    public function __construct(UserRepository $user_repository) {
        $this->user_repository = $user_repository;
    }

    public function run($ids, $toggle) {
        try {
            $users = $this->user_repository->findWhereIn('user_no', $ids);

            foreach ($users as $user) {
                if($toggle == false) {
                    $this->user_repository->update([
                        'hot_fimer' => 0
                    ], $user->user_no);
                } else {
                    $this->user_repository->update([
                        'hot_fimer' => 1
                    ], $user->user_no);
                }
            }

            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}

