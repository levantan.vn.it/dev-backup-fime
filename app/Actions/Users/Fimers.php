<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/12/2019
 * Time: 10:04 AM
 */

namespace App\Actions\Users;


use App\Actions\Action;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Criterias\Files\GetMainImageOfReviewsCriteria;
use App\Criterias\Review\GetNumberOfReviewsByUserIdsCriteria;
use App\Criterias\Review\GetReviewsByUserId;
use App\Criterias\User\GetHotFimersOrderByReviewCountCriteria;
use App\Criterias\User\GetNewFimersOrderByReviewCountCriteria;
use App\Criterias\UserFollow\GetFollowingOfCurrentUserCriteria;
use App\Criterias\Users\GetNumberOfFollowersByUserIdsCriteria;
use App\Criterias\Users\GetNumberOfFollowingsByUserIdsCriteria;

class Fimers extends Action
{
    protected $userRepository;
    protected $reviewRepository;
    protected $followRepository;
    protected $reviewFileRepository;

    public function __construct(UserRepositoryInterface $userRepository, ReviewRepositoryInterface $reviewRepository,
                                UserFollowRepositoryInterface $followRepository, ReviewFilesRepositoryInterface $reviewFileRepository)
    {
        $this->userRepository = $userRepository;
        $this->reviewRepository = $reviewRepository;
        $this->followRepository = $followRepository;
        $this->reviewFileRepository = $reviewFileRepository;
    }

    public function run($type, $limit = 20, $offset = 0)
    {
        $fimers = $this->getFimers($type, $limit, $offset);
        // get 5 review new of user
        $this->getReviewsOfFimer($fimers,$offset);
        // check current user follow fimer
        $this->getCurrentUserFollowing($fimers);

        return $fimers;
    }

    private function getFimers($type, $limit, $offset)
    {
        try {
            switch ($type) {
                case 'new_member':
                    $fimers = $this->userRepository->pushCriteria(new GetNewFimersOrderByReviewCountCriteria())->paginate(12);
                    break;
                case 'monthly':
                    $fimers = $this->userRepository->pushCriteria(new GetHotFimersOrderByReviewCountCriteria(30))->paginate(12);
                    break;
                case 'weekly':
                    $fimers = $this->userRepository->pushCriteria(new GetHotFimersOrderByReviewCountCriteria(7))->paginate(12);
                    break;
                case 'all':
                default:
                    $fimers = $this->userRepository->pushCriteria(new GetHotFimersOrderByReviewCountCriteria(356))->paginate(12);
                    break;
            }


            return $fimers;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * @param $fimers
     */
    private function getReviewsOfFimer($fimers,$offset)
    {
        $fimerIds = $fimers->pluck('user_no')->toArray();

        // $number_of_reviews = $this->reviewRepository->getByCriteria(new GetNumberOfReviewsByUserIdsCriteria($fimerIds))->keyBy('user_id');

        foreach ($fimers as $key => $fimer) {
            // $fimer->number_of_reviews = 0;
            // if (isset($number_of_reviews[$fimer->user_no])) {
            //     $fimer->number_of_reviews = $number_of_reviews[$fimer->user_no]->number_of_reviews;
            // }
            // get 5 review of fimer

            $reviews = $this->reviewRepository->getByCriteria(new GetReviewsByUserId($fimer->user_no, 5));

            // if((int)$_GET['page'] >= 10){
                $review_ids = $reviews->pluck('review_no')->toArray();
                $reviewsMainImage = $this->reviewFileRepository->getByCriteria(new GetMainImageOfReviewsCriteria($review_ids, true))->keyBy('review_no');

                foreach ($reviews as $review) {
                    $review->is_liked = 0;
                    if (isset($likes[$review->review_no])) {
                        $review->is_liked = 1;
                    }
                    if (isset($reviewsMainImage[$review->review_no])){
                        $review->main_image = $reviewsMainImage[$review->review_no];
                        if(!empty($review) && strtotime($review->writng_dt) < strtotime(date('2019-12-12 12:00:00'))){
                            
                            $file_data = public_path($review->file_cours.'/'.$review->stre_file_nm);
                            $file_thumb_name = str_replace("TMB", "THM", $review->stre_file_nm);
                            $image_thumb = \Image::make($file_data)->resize(100, null,function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                            });
                            $check = $image_thumb->save((public_path($review->file_cours.'/'.$file_thumb_name)));
                        }
                    }

                }
                $fimer->check = (int)$_GET['page'];
            // }
            
            $fimer->reviews = $reviews;
        }
    }

    protected function getCurrentUserFollowing($fimers)
    {
        $current_user_id = auth()->id();

        $user_ids = $fimers->pluck('user_no')->toArray();

        $user_followings = $this->followRepository->getByCriteria(
            new GetFollowingOfCurrentUserCriteria($user_ids, $current_user_id))->keyBy('followed_user_id');

        $followers = $this->followRepository->getByCriteria(
            new GetNumberOfFollowersByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        $followings = $this->followRepository->getByCriteria(
            new GetNumberOfFollowingsByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        foreach ($fimers as $fimer) {
            // if(!empty($fimer->pic)){
            //     $file_name = $fimer->pic;
            //     $extension = pathinfo($file_name, PATHINFO_EXTENSION);
            //     $name = pathinfo($file_name, PATHINFO_FILENAME);
            //     $new_url = 'https://api-np.fime.vn'.$file_name;
            //     $file_thumb_name = $name . '_TMB' . '.' . $extension;
            //     $image_thumb = \Image::make($new_url)->resize(150, null,function ($constraint) {
            //         $constraint->aspectRatio();
            //         $constraint->upsize();
            //     });
            //     $image_thumb->save((public_path().'/data/images/profile/'.$file_thumb_name));
                
            //     $fimer->update_pic = '/data/images/profile/'.$file_thumb_name;
            //     $fimer->save();
            // }
            
            $fimer->followed = 0;
            $fimer->number_of_followers = 0;
            $fimer->number_of_followings = 0;

            if (isset($followers[$fimer->user_no])) {
                $fimer->number_of_followers = $followers[$fimer->user_no]->number_of_followers;
            }

            if (isset($followings[$fimer->user_no])) {
                $fimer->number_of_followings = $followings[$fimer->user_no]->number_of_followings;
            }

            if (isset($user_followings[$fimer->user_no])) {
                $fimer->followed = 1;
            }
        }
        return $fimers;
    }
}
