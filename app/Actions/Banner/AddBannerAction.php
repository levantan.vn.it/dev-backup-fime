<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Banner;
use App\Actions\Action;
use App\Contracts\BannerRepositoryInterface;
use Mockery\Exception;

class AddBannerAction extends Action {
    protected $banner_repository;

    public function __construct(BannerRepositoryInterface $banner_repository) {
        $this->banner_repository = $banner_repository;
    }

    public function run($data) {
        try {
            $data['name'] = isset($data['name']) ? $data['name'] : null;
            $data['description'] = isset($data['description']) ? $data['description'] : null;
            $data['targetUrl'] = isset($data['targetUrl']) ? $data['targetUrl'] : null;
            $banner = $this->banner_repository->create($data);
            return $banner;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
