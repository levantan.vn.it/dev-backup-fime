<?php

namespace App\Actions\Blog;
use App\Actions\Action;
use App\Contracts\BlogRepositoryInterface;
use App\Criterias\Blog\GetAvailableBlogCriteria;

class GetAvailableAction extends Action {
    protected $repository;

    public function __construct(BlogRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run() {
        try {
            $blogs = $this->repository->getByCriteria(new GetAvailableBlogCriteria());
            return $blogs;
        } catch (\Exception $e) {
            \Log::error($e);
            return [];
        }
    }
}
