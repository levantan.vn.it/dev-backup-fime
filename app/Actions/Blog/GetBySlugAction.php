<?php

namespace App\Actions\Blog;

use App\Actions\Action;
use App\Contracts\BlogRepositoryInterface;
use Mockery\Exception;

class GetBySlugAction extends Action
{
    protected $repository;

    public function __construct(BlogRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function run($data)
    {
        try {
            $slug = $data['slug'];
            $blogs = $this->repository->findWhere(['slug' => $slug, 'is_disabled' => 0]);
            $blog = $blogs[0];

            // Add one views
            $blog = $this->repository->update([
                'views' => $blog->views + 1
            ], $blog->id);
            return $blog;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
