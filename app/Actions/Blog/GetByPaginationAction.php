<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Blog;
use App\Contracts\BlogRepositoryInterface;
use App\Actions\Action;
use Carbon\Carbon;

class GetByPaginationAction extends Action {
    protected $repository;

    public function __construct(BlogRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($params) {
        $blogs = $this->repository->scopeQuery(function($query) use ($params){
            if(isset($params['title']) && $params['title'] != "null") {
                $query = $query->where('title', 'like', '%' . $params['title'] . '%');
            }

            if(isset($params['is_disabled']) && $params['is_disabled'] != "null") {
                $query = $query->where('is_disabled', '=', $params['is_disabled']);
            }

            if(isset($params['show_on_main']) && $params['show_on_main'] != "null") {
                $query = $query->where('show_on_main', '=', $params['show_on_main']);
            }

            if(isset($params['from']) && $params['from'] != "null") {
                $query = $query->where('created_at', '>=', Carbon::parse($params['from']));
            }

            if(isset($params['to']) && $params['to'] != "null") {
                $query = $query->where('created_at', '<', Carbon::parse($params['to'])->addDay(1));
            }

            return $query->orderBy($params['column'], $params['sort']);
        })->paginate($params['pageSize']);
       return $blogs;
    }
}
