<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Blog;

use App\Actions\Action;
use App\Contracts\BlogRepositoryInterface;
use Mockery\Exception;

class AddBlogAction extends Action {
    protected $repository;

    public function __construct(BlogRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($data) {
        try {
            $blog = $this->repository->create($data);
            return $blog;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
