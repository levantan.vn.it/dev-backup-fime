<?php

/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/2/2019
 * Time: 4:37 PM
 */

namespace App\Actions\Brand;


use App\Actions\Action;
use App\Contracts\TipsRepositoryInterface;
use App\Criterias\Tip\GetAllTipsByCriteria;
use App\Models\Tips;

class GetTipBrandAction extends Action
{
    protected $tipsRepository;

    public function __construct(TipsRepositoryInterface $tipsRepository)
    {
        $this->tipsRepository = $tipsRepository;
    }

    public function run($id)
    {
    //     $period = isset($params['period']) ? $params['period'] : 0;
    //     $pageSize = isset($params['pageSize']) ? $params['pageSize'] : 0;
        // if ($pageSize != 0) {
            $model = Tips::select('ti_id',
                'subject',
                'updated_at',
                'img_url',
                'cover_img1',
                'cover_img2',
                'cover_img3',
                'cover_img4',
                'tips_desc_head',
                'tips_desc_body',
                'created_at'
                )
                ->orderBy('ti_id', 'desc')
                ->where('display_yn', 'Y')
                ->where('code_brand', $id)
                ->skip((\Request::input('page') - 1) * 10)
                ->limit(10)->get();
        // } 
        return $model;
    }
}
