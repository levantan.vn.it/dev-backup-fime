<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Brand;
use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;
use App\Criterias\Brand\GetAllBrandCriteria;
use App\Actions\Constant;
class GetAllAction extends Action {
    protected $repository;

    public function __construct(CodeRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($params) {
	    if(!empty($params['pageSize']) && $params['pageSize'] != "null"){
	    	$params['code_group']= Constant::$BRAND_GROUP_CODE;
	    	$brands = $this->repository->scopeQuery(function($query) use ($params){
	    		if(isset($params['name']) && $params['name'] != "null") {
	    		    $query = $query->where('code_nm', 'like', '%' . $params['name'] . '%');
	    		}
	    	    $query = $query->where('code_group', $params['code_group']);
	    	    $query = $query->where('use_at', '=', 'Y');
	    	    return $query;
	    	})->orderBy('code_nm', 'ASC')->paginate($params['pageSize']);
	    }else{
	    	$brands = $this->repository->getByCriteria(new GetAllBrandCriteria());
	    }
        return $brands;
    }
}
