<?php

namespace App\Actions\Review;

use App\Actions\Action;
use App\Contracts\ReviewCommentRepositoryInterface;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Contracts\ReviewLikeRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Criterias\Comment\GetNumberOfCommentsByReviewIdsCriteria;
use App\Criterias\Files\GetReviewFilesCriteria;
use App\Criterias\TryFree\GetTryNameCriteria;
use App\Criterias\UserFollow\GetFollowingOfCurrentUserCriteria;
use App\Criterias\UserLike\GetNumberOfLikesByReviewIdsCriteria;
use App\Criterias\UserLike\GetReviewLikeInfoCriteria;
use App\Criterias\Users\GetNumberOfFollowersByUserIdsCriteria;
use App\Criterias\Users\GetNumberOfFollowingsByUserIdsCriteria;
use App\Criterias\Users\GetUsersByIdsCriteria;
use Mockery\Exception;

class GetReviewDetailsAction extends Action
{
    protected $review_repository;
    private $user_follow_repository;
    private $user_like_repository;
    private $comment_repository;
    private $user_repository;
    private $review_files_repository;
    private $try_repository;

    public function __construct(
        ReviewRepositoryInterface $review_repository,
        UserFollowRepositoryInterface $user_follow_repository,
        ReviewLikeRepositoryInterface $user_like_repository,
        ReviewCommentRepositoryInterface $comment_repository,
        UserRepositoryInterface $user_repository,
        ReviewFilesRepositoryInterface $review_files_repository,
        TryRepositoryInterface $try_repository
    ) {
        $this->review_repository = $review_repository;
        $this->user_follow_repository = $user_follow_repository;
        $this->user_like_repository = $user_like_repository;
        $this->comment_repository = $comment_repository;
        $this->user_repository = $user_repository;
        $this->review_files_repository = $review_files_repository;
        $this->try_repository = $try_repository;
    }

    protected function decorateData($review)
    {
        $this->checkCurrentUserLikedReview($review);
        $this->getFiles($review);
        return $review;
    }

    protected function getFiles($review)
    {
        try {
            $files = $this->review_files_repository->getByCriteria(new GetReviewFilesCriteria($review->review_no));
            $review->files = $files;
            return $review;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function checkCurrentUserLikedReview($review)
    {
        $current_user_id = auth()->id();
        $user_liked = $this->user_like_repository->findWhereIn('review_no', [$review->review_no])->keyBy('user_no');

        if (isset($user_liked[$current_user_id])) {
            $review->is_liked = 1;
        } else {
            $review->is_liked = 0;
        }

        return $review;
    }

    public function getGoodNameOfReview($cntnts_no)
    {
        $data = $this->try_repository->getByCriteria(new GetTryNameCriteria($cntnts_no));
        if (count($data) > 0) {
            return $data[0]->sj;
        }
    }

    public function run($slug)
    {

        try {

            if (auth()->user() && auth()->user()->role_id === 1) {
                $review = $this->review_repository->findWhere(['slug' => $slug])->first();
            } else {
                $review = $this->review_repository->findWhere(['slug' => $slug, 'expsr_at' => "Y", 'delete_at' => 'N'])->first();
            }


            if ($review == null)
                return null;

            if (($review->goods_nm == '' || $review->goods_nm == null) && $review->cntnts_no != null) {
                $review->goods_nm = $this->getGoodNameOfReview($review->cntnts_no);
            }
            $review = $this->decorateData($review);
            $author = $this->user_repository->getByCriteria(new GetUsersByIdsCriteria([$review->user_no]))[0];
            if ($author) {
                $review->author = $author;
            } else {
                $review->author = ['id' => 1];
            }

            
            return $review;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }
   
}
