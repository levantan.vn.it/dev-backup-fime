<?php

namespace App\Actions\Review;

use App\Actions\UserPoint\AddPointsToUserAction;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\FilesRepositoryInterface;
use App\Contracts\HashtagRepositoryInterface;
use App\Contracts\ReviewCommentRepositoryInterface;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Contracts\ReviewHashTagsRepositoryInterface;
use App\Contracts\ReviewLikeRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\UserPointRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;

class RestoreReviewAction extends AddPointsToUserAction
{
    protected $review_repository;
    protected $user_like_repository;
    protected $comment_repository;
    protected $hashtag_repository;
    protected $user_repository;
    protected $file_repository;
    protected $review_files_repository;
    protected $reviewHashTagsRepository;

    public function __construct(ReviewRepositoryInterface $review_repository,
                                UserRepositoryInterface $user_repository,
                                ReviewCommentRepositoryInterface $comment_repository,
                                ReviewLikeRepositoryInterface $user_like_repository,
                                HashtagRepositoryInterface $hashtag_repository,
                                FilesRepositoryInterface $files_repository,
                                ReviewFilesRepositoryInterface $review_files_repository,
                                ReviewHashTagsRepositoryInterface $reviewHashTagsRepository,
                                CodeRepositoryInterface $codeRepository,
                                UserPointRepositoryInterface $user_point_repository
    )
    {
        $this->user_repository = $user_repository;
        $this->review_repository = $review_repository;
        $this->user_like_repository = $user_like_repository;
        $this->comment_repository = $comment_repository;
        $this->hashtag_repository = $hashtag_repository;
        $this->file_repository = $files_repository;
        $this->review_files_repository = $review_files_repository;
        $this->reviewHashTagsRepository = $reviewHashTagsRepository;
        parent::__construct($user_point_repository, $codeRepository);
    }

    public function run($ids)
    {
        try {
            foreach ($ids as $id) {

                $review = $this->review_repository->find($id);
                if ($review->user_no == auth()->id() || Auth::user()->role_id == 1) {

                    $this->review_repository->update([
                        'delete_at' => 'N'
                    ], $id);
                    $user = Auth::user();

                    $this->user_repository->update([
                        'reviews' => $user->reviews + 1
                    ], $user->user_no);
                } else {
                    return false;
                }
            }
            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
