<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\UserTry;

use App\Actions\Notification\CreateNotificationAction;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\NotificationRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserPointRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class UpdateDeliveryInfoAction extends CreateNotificationAction {
    protected $user_try_repository;

    public function __construct(UserTryRepositoryInterface $user_try_repository,
                                NotificationRepositoryInterface $notification_repository,
                                ReviewRepositoryInterface $review_repository,
                                TryRepositoryInterface $try_repository,
                                UserPointRepositoryInterface $user_point_repository,
                                CodeRepositoryInterface $codeRepository) {
        $this->user_try_repository = $user_try_repository;
        parent::__construct($notification_repository, $review_repository, $try_repository, $user_point_repository, $codeRepository);
    }

    public function run($data) {
        try {
            $cntnts_no = $data['cntnts_no'];
            $user_no = $data['user_no'];
            $dlvy_dt = $data['dlvy_dt'];
            $dlvy_cmpny_code = $data['dlvy_cmpny_code'];
            $dlvy_mth_code = $data['dlvy_mth_code'];
            $invc_no = $data['invc_no'];

            $data = DB::table('TCT_DRWT')
                ->where('cntnts_no', $cntnts_no)
                ->where('user_no', $user_no)
                ->update([
                    'dlvy_at' => 'Y',
                    'dlvy_dt' => $dlvy_dt,
                    'dlvy_cmpny_code' => $dlvy_cmpny_code,
                    'dlvy_mth_code' => $dlvy_mth_code,
                    'invc_no' => $invc_no
                ]);

            $this->createDeliveryNotification($cntnts_no, $user_no, $dlvy_dt);
            return $data;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
