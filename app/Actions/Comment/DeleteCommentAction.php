<?php

namespace App\Actions\Comment;

use App\Actions\Action;
use App\Actions\UserPoint\AddPointsToUserAction;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\CommentRepositoryInterface;
use App\Contracts\ReviewCommentRepositoryInterface;
use App\Contracts\UserPointRepositoryInterface;
use App\Criterias\Comment\GetCommentNumberByTryIdsCriteria;
use App\Criterias\Comment\GetNumberOfCommentsByReviewIdsCriteria;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;
use App\Models\TryFree;
use App\Models\Review;
class DeleteCommentAction extends AddPointsToUserAction
{
    protected $comment_repository;
    protected $review_comment_repository;

    public function __construct(
        CommentRepositoryInterface $comment_repository,
        ReviewCommentRepositoryInterface $review_comment_repository,
        UserPointRepositoryInterface $userPointRepository,
        CodeRepositoryInterface $codeRepository
    ) {
        $this->comment_repository = $comment_repository;
        $this->review_comment_repository = $review_comment_repository;
        parent::__construct($userPointRepository, $codeRepository);
    }

    protected function getCommentNumberOfReview($review)
    {
        try {
            $list_comment_number = $this->review_comment_repository->getByCriteria(new GetNumberOfCommentsByReviewIdsCriteria([$review->review_no]));
            $list_comment_number = collect($list_comment_number)->keyBy('review_no');

            if (isset($list_comment_number[$review->review_no])) {
                $review->comment_number = $list_comment_number[$review->review_no]->comment_number;
            } else {
                $review->comment_number = 0;
            }
            return $review;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    protected function getNumberCommentsOfTry($try)
    {
        $list_try_comments = $this->comment_repository->getByCriteria(new GetCommentNumberByTryIdsCriteria([$try->cntnts_no]));

        $comments = 0;
        if (count($list_try_comments) > 0) {
            $comments = $list_try_comments[0]->comment_number;
        }

        $try->comment_number = $comments;
        return $try;
    }

    public function run($id, $type)
    {
        \DB::beginTransaction();
        try {
            if ($type == 'try') {
                if (is_array($id)) {
                    foreach ($id as $key) {
                        $comment = $this->comment_repository->find($key);
                        // get detail try free
                        $try = TryFree::select('cntnts_no','comments')->where('cntnts_no',$comment->cntnts_no)->first();
                        // get current comment
                        $current_comment = $try->comments;
                        // minus current comment
                        $minus = 1;
                        if ($comment->user_no == auth()->id() || Auth::user()->role_id == 1) {
                            // $result = $this->comment_repository->update([
                            //     'delete_at' => 'Y'
                            // ], $comment->id);

                            // Delete replies of this comment
                            $replies = $this->comment_repository->findWhere(['parent_id' => $key]);
                            // minus total all reply of comment
                            $minus += count($replies);
                            foreach ($replies as $reply) {
                                $reply->delete();
                            }
                            // $result = $this->getNumberCommentsOfTry($try);
                            $this->comment('try', $comment->cntnts_no, $comment->user_no, true);
                            // update comments of try
                            $current_comment = $current_comment - $minus;
                            $try->comments = $current_comment;
                            $try->save();
                            $comment->delete();
                            $try->comment_number =$current_comment;
                            $result = $try;
                        } else {
                            return false;
                        }
                    }
                } else {
                    $comment = $this->comment_repository->find($id);
                    // get detail try free
                    $try = TryFree::select('cntnts_no','comments')->where('cntnts_no',$comment->cntnts_no)->first();
                    // get current comment
                    $current_comment = $try->comments;
                    // minus current comment
                    $minus = 1;
                    if ($comment->user_no == auth()->id() || Auth::user()->role_id == 1) {
                        

                        // Delete replies of this comment
                        $replies = $this->comment_repository->findWhere(['parent_id' => $id]);
                        // minus total all reply of comment
                        $minus += count($replies);
                        foreach ($replies as $reply) {
                            $reply->delete();
                        }
                        // $result = $this->getNumberCommentsOfTry($result);
                        $this->comment('try', $comment->cntnts_no, $comment->user_no, true);
                        // update comments of try
                        $current_comment = $current_comment - $minus;
                        $try->comments = $current_comment;
                        $try->save();
                        $comment->delete();
                        $try->comment_number = $current_comment;
                        $result = $try;
                    } else {
                        return false;
                    }
                }
            } else {
                if (is_array($id)) {
                    foreach ($id as $key) {
                        $comment = $this->review_comment_repository->find($key);
                        if ($comment->user_no == auth()->id() || Auth::user()->role_id == 1) {
                            // $result = $this->review_comment_repository->update([
                            //     'delete_at' => 'Y'
                            // ], $comment->id);

                            // Delete replies of this comment
                            // minus current comment
                            $minus = 1;
                            $replies = $this->review_comment_repository->findWhere(['parent_id' => $key]);
                            // minus total all reply of comment
                            $minus += count($replies);
                            foreach ($replies as $reply) {
                                $reply->delete();
                            }

                            // $result = $this->getCommentNumberOfReview($result);
                            $this->comment('review', $comment->review_no, $comment->user_no, true);
                            $comment->delete();
                            // get detail review
                            $review = Review::select('review_no','comments')->where('review_no',$comment->review_no)->first();
                            $current_comment = $review->comments - $minus;
                            $review->comments = $current_comment;
                            $review->save();
                            $review->comment_number = $current_comment;
                            $result = $review;
                        } else {
                            return false;
                        }
                    }
                } else {
                    $comment = $this->review_comment_repository->find($id);
                    if ($comment->user_no == auth()->id() || Auth::user()->role_id == 1) {
                        // $result = $this->review_comment_repository->update([
                        //     'delete_at' => 'Y'
                        // ], $comment->id);
                        // minus current comment
                        $minus = 1;
                        // Delete replies of this comment
                        $replies = $this->review_comment_repository->findWhere(['parent_id' => $id]);
                        // minus total all reply of comment
                        $minus += count($replies);
                        foreach ($replies as $reply) {
                            $reply->delete();
                        }

                        // $result = $this->getCommentNumberOfReview($result);
                        $this->comment('review', $comment->review_no, $comment->user_no, true);
                        $comment->delete();
                        // get detail review
                        $review = Review::select('review_no','comments')->where('review_no',$comment->review_no)->first();
                        $current_comment = $review->comments - $minus;
                        $review->comments = $current_comment;
                        $review->save();
                        $review->comment_number = $current_comment;
                        $result = $review;
                    } else {
                        return false;
                    }
                }
            }
            \DB::commit();
            return $result;
        } catch (Exception $e) {
            \DB::rollBack();
            \Log::error($e);
        }
    }
}
