<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/1/2019
 * Time: 2:03 PM
 */

namespace App\Actions\ExcelExport;


use App\Actions\Action;
use App\Actions\Constant;
use App\Contracts\UserRepositoryInterface;
use App\Contracts\UserPointRepositoryInterface;
use App\Contracts\RoleRepositoryInterface;
use Carbon\Carbon;

class UserAction extends Action
{
    protected $user_repository;
    protected $userPointRepository;
    protected $roleRepository;

    /**
     * TriesAction constructor.
     * @param UserRepositoryInterface $try_repository
     */
    public function __construct(UserRepositoryInterface $user_repository,
                                UserPointRepositoryInterface $userPointRepository,
                                RoleRepositoryInterface $roleRepository)
    {
        $this->user_repository = $user_repository;
        $this->userPointRepository = $userPointRepository;
        $this->roleRepository = $roleRepository;
    }

    public function run($params)
    {
        $users = $this->user_repository->scopeQuery(function ($query) use ($params) {
            $searchType = isset($params['searchType']) ? $params['searchType'] : '';
            $searchValue = isset($params['searchValue']) ? $params['searchValue'] : '';
            $allowComment = isset($params['allowComment']) ? $params['allowComment'] : 1;
            $allowReview = isset($params['allowReview']) ? $params['allowReview'] : 1;
            $isActive = isset($params['isActive']) ? ($params['isActive'] === 'true' ? 'N' : 'Y') : 'N';
            $isDelete = isset($params['isDelete']) ? ($params['isDelete'] === 'true' ? 'Y' : 'N') : 'N';
            $role = isset($params['role']) ? $params['role'] : 'fimer';
            $role_id = $this->roleRepository->findByField('name', $role)->first();

            if (isset($searchValue) && $searchValue != '') {
                $query = $query->where($searchType, 'like', '%' . $searchValue . '%');
            }
            if (isset($role) && $role != "null") {
                $query = $query->where('TDM_USER.role_id','=', $role_id->id);
            }
            $query = $query->select('TDM_USER.user_no',
            'TDM_USER.reg_name',
            'TDM_USER.id',
            'TDM_USER.cellphone',
            'TDM_USER.email',
            'TDM_USER.home_addr1',
            'TDM_USER.last_login_dt',
            'TDM_USER.reviews'
            );
            $query = $query->where('TDM_USER.allow_comment','=',$allowComment);
            $query = $query->where('TDM_USER.allow_review','=',$allowReview);
            $query = $query->where('TDM_USER.delete_at','=',$isDelete);
            $query = $query->where('TDM_USER.drmncy_at','=',$isActive);
            return $query->orderBy('reviews', 'desc');
        })->paginate($params['pageSize']);
        $rs = [];
        $this->getUsersPoint($users);    
        foreach ($users as $item) {
            $data = [
                'user_no' => $item->user_no,
                'reg_name' => $item->reg_name,
                'id' => isset($item->id) ? $item->id : '',
                'cellphone' => isset($item->cellphone) ? $item->cellphone : '',
                'email' => isset($item->email) ? $item->email : '',
                'home_addr1' => isset($item->home_addr1) ? $item->home_addr1 : '',
                'last_login_dt' => isset($item->last_login_dt) ? $item->last_login_dt : '',
                'total_point' => isset($item->total_point) ? $item->total_point : '',
                'reviews' => isset($item->reviews) ? $item->reviews : '',
            ];
            array_push($rs, $data);
        }

        return $rs;
    }

    public function getUsersPoint($users){
        $user_ids = $users->pluck('user_no')->toArray();

        $points = $this->userPointRepository->scopeQuery(function ($query) use ($user_ids){
            $query = $query->select(\DB::raw('sum(accml_pntt) as total_point'), 'user_no');
            $query = $query->join('TSM_CODE', 'TCT_PNTT.pntt_accml_code', '=', 'TSM_CODE.code');
            $query = $query->whereIn('user_no', $user_ids);
            $query = $query->where('TSM_CODE.code_group', 405);
            $query = $query->where('TSM_CODE.use_at', 'Y');
            $query = $query->groupBy('user_no');
            return $query;
        });

        $points = $points->all()->keyBy('user_no');
        foreach ($users as $user) {
            if (isset($points[$user->user_no])) {
                $user->total_point = $points[$user->user_no]->total_point;
            } else {
                $user->total_point = 0;
            }
        }
    }
}
