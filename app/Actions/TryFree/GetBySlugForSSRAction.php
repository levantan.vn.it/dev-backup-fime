<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\TryFree;

use App\Actions\Action;
use App\Contracts\TryExtRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Criterias\TryFree\GetFilesOfTryCriteria;
use App\Criterias\TryFree\GetTrySSRDetail;
use Mockery\Exception;

class GetBySlugForSSRAction extends Action {
    protected $repository;
    protected $ext_repository;

    public function __construct(TryRepositoryInterface $repository,
                                TryExtRepositoryInterface $ext_repository
    ) {
        $this->repository = $repository;
        $this->ext_repository = $ext_repository;
    }

    public function run($slug) {
        try {
            $try_ext = $this->ext_repository->findWhere(['slug' => $slug, 'expsr_at' => 'Y', 'delete_at' => 'N'])->first();
            if($try_ext) {
                $try = $this->repository->getByCriteria(new GetTrySSRDetail($try_ext->cntnts_no))->first();
                if ($try_ext == null || $try == null) {
                    return null;
                }
                $try->cntnts_nm = $try_ext->sj;
                $try->files = $this->repository->getByCriteria(new GetFilesOfTryCriteria($try_ext->cntnts_no));
                return $try;
            }
            return null;
        } catch (Exception $e) {
            \Log::error($e);
            return null;
        }
    }
}
