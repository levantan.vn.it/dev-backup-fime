<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\TryFree;
use App\Contracts\TryRepositoryInterface;
use App\Actions\Action;

class GetAllAction extends Action {
    protected $try_repository;

    public function __construct(TryRepositoryInterface $try_repository) {
        $this->try_repository = $try_repository;
    }

    public function run() {
       return ['active' => true];
    }
}
