<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\TryFree;

use App\Actions\Action;
use App\Contracts\HashtagRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Criterias\TryFree\GetFilesOfTryInAdminCriteria;
use App\Criterias\TryFree\GetImageDescOfTryCriteria;
use Carbon\Carbon;

class GetByAction extends Action {
    protected $repository;
    protected $hashtag_repository;

    public function __construct(TryRepositoryInterface $repository,
                                HashtagRepositoryInterface $hashtag_repository) {
        $this->repository = $repository;
        $this->hashtag_repository = $hashtag_repository;
    }

    public function run($id) {
        $result = $this->repository->scopeQuery(function($query) use ($id){
            $query = $query->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no');

            $query = $query->select('TCT_GOODS.*', 'TOM_CNTNTS_WDTB.sj AS cntnts_nm', 'TOM_CNTNTS_WDTB.regist_dt AS created_at',
                'TOM_CNTNTS_WDTB.expsr_at AS expsr_at');

            return $query->where('TCT_GOODS.cntnts_no', $id);
        })->first();

        $images = $this->repository->getByCriteria(new GetFilesOfTryInAdminCriteria($id));

        $result->files = $images;
        $result->imgDesc = $this->repository->getByCriteria(new GetImageDescOfTryCriteria($id));
        $result->goods_dc = nl2br($result->goods_dc);
        $start_time = strtotime(date("Y-m-d H:i:s", strtotime($result->event_bgnde)));
        $result->event_bgnde = date("Y-m-d H:i:s", strtotime('-7 hours', $start_time));
        $end_time = strtotime(date("Y-m-d H:i:s", strtotime($result->event_endde)));
        $result->event_endde = date("Y-m-d H:i:s", strtotime('-7 hours', $end_time));
        // $result->event_bgnde = strtotime($result->event_bgnde);
        // $result->event_bgnde = Carbon::parse($result->event_bgnde)->subHours(7);
        // $result->event_endde = Carbon::parse($result->event_endde)->subHours(7);
       return $result;
    }
}
