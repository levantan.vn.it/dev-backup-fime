<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\TryFree;

use App\Actions\Action;
use App\Contracts\TryExtRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserLikeRepositoryInterface;
use Mockery\Exception;

class ToggleTryAction extends Action {
    protected $repository;

    public function __construct(TryExtRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($ids, $toggle) {
        try {
            $tries = $this->repository->findWhereIn('cntnts_no', $ids);

            foreach ($tries as $try) {
                if($toggle == false) {
                    $this->repository->update([
                        'expsr_at' => 'Y'
                    ], $try->cntnts_no);
                } else {
                    $this->repository->update([
                        'expsr_at' => 'N'
                    ], $try->cntnts_no);
                }
            }

            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
