<?php

namespace App\Models;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class TryExt extends BaseModel
{
    protected $connection = 'aws';

    use HasSlug;

    protected  $primaryKey = 'cntnts_no';

    protected $table = 'TOM_CNTNTS_WDTB';

    public $timestamps = false;

    public $incrementing = false;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cntnts_no', 'ctg_no', 'slug', 'sj', 'wdtb_cours', 'search_kwrd', 'othbc_grad_code', 'chrg_instt', 'origin_cpyrht',
        'delete_at', 'expsr_at', 'svc_dt' ];

    /**
     * @return SlugOptions
     */
    public function getSlugOptions()
    {
        return SlugOptions::create()
            ->generateSlugsFrom('sj')
            ->saveSlugsTo('slug');
    }
}
