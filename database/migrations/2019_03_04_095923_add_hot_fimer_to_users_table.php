<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHotFimerToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('TDM_USER', 'hot_fimer_7days')) {

            Schema::table('TDM_USER', function (Blueprint $table) {
                $table->integer('hot_fimer_7days')->default(0);
                $table->integer('hot_fimer_30days')->default(0);
                $table->integer('hot_fimer_90days')->default(0);
                $table->integer('hot_fimer_1year')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TDM_USER', function(Blueprint $table) {
            $table->dropColumn('hot_fimer_7days');
            $table->dropColumn('hot_fimer_30days');
            $table->dropColumn('hot_fimer_90days');
            $table->dropColumn('hot_fimer_1year');
        });
    }
}
