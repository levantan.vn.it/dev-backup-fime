<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('banners')) {

            Schema::create('banners', function (Blueprint $table) {
                $table->increments('id');

                $table->string('name');
                $table->string('description', 255)->nullable();
                $table->string('url');
                $table->unsignedInteger('resource_type')->nullable();
                $table->string('target_url');
                $table->string('target_type')->default('_blank');
                $table->string('button_text');
                $table->boolean('is_disabled')->nullable()->default(false);
                $table->dateTime('period_from')->nullable();
                $table->dateTime('period_to')->nullable();

                $table->unsignedInteger('created_by')->nullable();
                $table->unsignedInteger('updated_by')->nullable();
                $table->unsignedInteger('deleted_by')->nullable();

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
