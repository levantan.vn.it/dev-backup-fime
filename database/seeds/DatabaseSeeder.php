<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(UsersTableSeeder::class);
//        $this->call(RolesAndPermissionsSeeder::class);

//        foreach (\App\Models\User::all() as $user) {
//            if (!empty($user->id)) {
//                $user->slug = str_slug($user->id, '-');
//                $user->save();
//            } else {
//                $user->slug = str_slug($user->reg_name, '-');
//                $user->save();
//            }
//        }
//
//        foreach (\App\Models\Review::all() as $review) {
//            $review->review_dc = $review->review_dc . ' - ';
//            $review->save();
//        }
    }
}

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class, 100)->create();
    }
}
